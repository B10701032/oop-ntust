#include "Month.h"
#include <string>
#include <iostream>
using namespace std;

Month::Month()
{
	month = 1;
}

Month::Month(char first, char second, char third)
{
	//cout << "const month str " << first << second << third << endl;
	if (first == 'J') {
		if (second == 'u') {
			if (third == 'n') month = 6;
			else if (third == 'l') month = 7;
		}
		else if (second == 'a' && third == 'n') month = 1;
	}
	else if (first == 'M') {
		if (second == 'a' && third == 'r') month = 3;
		else if (second == 'a' && third == 'y') month = 5;
	}
	else if (first == 'A') {
		if (second == 'p' && third == 'r') month = 4;
		else if (second == 'u' && third == 'g') month = 8;
	}
	else if (first == 'F' && second == 'e' && third == 'b') month = 2;
	else if (first == 'S' && second == 'e' && third == 'p') month = 9;
	else if (first == 'O' && second == 'c' && third == 't') month = 10;
	else if (first == 'N' && second == 'o' && third == 'v') month = 11;
	else if (first == 'D' && second == 'e' && third == 'c') month = 12;
	else month = 1;
}

Month::Month(int monthInt)
{
	//cout << "const month int " << monthInt << endl;
	if (monthInt >= 1 && monthInt <= 12) {
		month = monthInt;
	}
	else month = 1;
}

Month::~Month()
{

}

void Month::inputInt()
{
	int mon;
	cin >> mon;
	if (mon >= 1 and mon <= 12) {
		month = mon;
	}
	else month = 1;
}

void Month::inputStr()
{	
	char first, second, third;
	cin >> first >> second >> third;
	if (first == 'J') {
		if (second == 'u') {
			if (third == 'n') month = 6;
			else if (third == 'l') month = 7;
		}
		else if (second == 'a' && third == 'n') month = 1;
	}
	else if (first == 'M') {
		if (second == 'a' && third == 'r') month = 3;
		else if (second == 'a' && third == 'y') month = 5;
	}
	else if (first == 'A') {
		if (second == 'p' && third == 'r') month = 4;
		else if (second == 'u' && third == 'g') month = 8;
	}
	else if (first == 'F' && second == 'e' && third == 'b') month = 2;
	else if (first == 'S' && second == 'e' && third == 'p') month = 9;
	else if (first == 'O' && second == 'c' && third == 't') month = 10;
	else if (first == 'N' && second == 'o' && third == 'v') month = 11;
	else if (first == 'D' && second == 'e' && third == 'c') month = 12;
	else month = 1;
}

void Month::outputInt()
{	
	if (month <= 12 && month >= 1) cout << month;
	else cout << 1;
}

void Month::outputStr()
{
	if (month == 1) cout << "Jan";
	else if (month == 2) cout << "Feb";
	else if (month == 3) cout << "Mar";
	else if (month == 4) cout << "Apr";
	else if (month == 5) cout << "May";
	else if (month == 6) cout << "Jun";
	else if (month == 7) cout << "Jul";
	else if (month == 8) cout << "Aug";
	else if (month == 9) cout << "Sep";
	else if (month == 10) cout << "Oct";
	else if (month == 11) cout << "Nov";
	else if (month == 12) cout << "Dec";
	else cout << "Jan";
}

Month Month::nextMonth()
{
	return Month(month + 1);
}
