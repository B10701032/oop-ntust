#include <iostream>
#include <cstring>
#include "HotDogStand.h"
using namespace std;


int HotDogStand::totalSellAmount = 0;
HotDogStand::HotDogStand()
{
	HotDogStand::hotDogSellAmount = 0;
	HotDogStand::totalSellAmount += 0;
}

HotDogStand::HotDogStand(const char * id)
{
	//HotDogStand::standId = id;
	HotDogStand::standId = new char[strlen(id)];
	strcpy(HotDogStand::standId, id);
	HotDogStand::hotDogSellAmount = 0;
	HotDogStand::totalSellAmount += 0;
	//std::cout << HotDogStand::standId;
}

HotDogStand::HotDogStand(const char * id, int amount)
{
	HotDogStand::standId = new char[strlen(id)];
	strcpy(HotDogStand::standId, id);
	HotDogStand::totalSellAmount += amount;
	HotDogStand::hotDogSellAmount = amount;

}


HotDogStand::~HotDogStand()
{
}

void HotDogStand::justSold()
{
	HotDogStand::hotDogSellAmount++;
	HotDogStand::totalSellAmount++;
}

void HotDogStand::print()
{
	cout << HotDogStand::standId << " " << HotDogStand::hotDogSellAmount << endl;
}

int HotDogStand::thisStandSoldAmount()
{
	return HotDogStand::hotDogSellAmount;
}

int HotDogStand::allStandSoldAmount()
{
	return HotDogStand::totalSellAmount;
}
