#include <iostream>
using namespace std;

class Character
{
public:
	int hp;
	int attack;
	int defense;
	Character() : hp(0), attack(0), defense(0) {}
	Character(int h, int a, int d) : hp(h), attack(a), defense(d) {}
};

int main() 
{
	int playerH = 0, playerA = 0, playerD = 0;
	cin >> playerH >> playerA >> playerD;
	Character player(playerH, playerA, playerD);

	
	int mH = 0, mA = 0, mD = 0;
	bool gaming = true;
	while (cin >> mH >> mA >> mD)
	{
		Character monster(mH, mA, mD);
		int playerAV = player.attack - monster.defense;
		if (playerAV < 0)
			playerAV = 0;
		int monsterAV = monster.attack - player.defense;
		if (monsterAV < 0)
			monsterAV = 0;

		bool battling = true;
		while (battling)
		{
			monster.hp -= playerAV;
			cout << "Player:" << player.hp << " Enemy:" << monster.hp << endl;
			if (monster.hp <= 0)
			{
				cout << "Player Win" << endl;
				battling = false;
				break;
			}

			player.hp -= monsterAV;
			cout << "Player:" << player.hp << " Enemy:" << monster.hp << endl;
			if (player.hp <= 0)
			{
				cout << "Player Dead" << endl;
				battling = false;
				gaming = false;
				break;
			}
		}

		if (gaming == false)
			break;
	}

    return 0;
}