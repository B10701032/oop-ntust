#pragma once
#include <iostream>
#include "Car.h"
#include "Circle.h"
#include "Cylinder.h"
#include "Wheel.h"
using namespace std;

#define PI 3.14159

class Train
{
private:
	int carCnt;
	double carLen;
	double carWid;
	double carHei;
	int wheelCnt;
	double tireRad;
	double tireWid;
	double axleRad;
	double axleWid;
	int tireCnt;
	int axleCnt;

public:
	Train();
	Train(int carCnt, double carLen, double carWid, double carHei,
		int wheelCnt, double tireRad, double tireWid, double axleRad, double axleWid);
	void input();         //Get input
	double getVolume();  //Return class volume
};