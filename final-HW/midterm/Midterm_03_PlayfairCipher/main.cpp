#include <iostream>
#include <cstring>
#include <string>
using namespace std;

string PlayfairCipher(string& plaintext, string& key);

int main() 
{
	string plaintext, key;
	while (cin >> plaintext >> key)
	{
		cout << PlayfairCipher(plaintext, key) << endl;
	}
    return 0;
}

string PlayfairCipher(string& plaintext, string& key)
{
	// 先把 I 和 J 視為相同物
	for (int i = 0; i < key.length(); i++)
		if (key[i] == 'j')
			key[i] = 'i';
	// 把重複的key刪掉
	bool alpha[26] = { 0 }; // 1 是已經存在 0 是不存在
	string tempKey = "";
	for (int i = 0; i < key.length(); i++)
	{
		if (alpha[key[i] - 'a'] == 0)
		{
			tempKey += key[i];
			alpha[key[i] - 'a'] = 1;
		}

		if (key[i] - 'a' == 'i')
		{
			alpha['j' - 'a'] = 1;
		}
	}
	key = tempKey;
	string table[5][5];
	for (int i = 0; i < 5; i++)
	{
		for (int j = 0; j < 5; j++)
			table[i][j] = "NULL";
	}

	// 填入刪除重複和取代j之後的 key
	for (int i = 0; i < key.length(); i++)
	{
		int row = i / 5;
		int col = i % 5;
		table[row][col] = key[i];
	}

	// 補上還沒有用到的字母
	for (int i = 0; i < 5; i++)
	{
		for (int j = 0; j < 5; j++)
		{
			if (table[i][j] == "NULL")
			{
				for (int k = 0; k < 26; k++)
				{
					if (alpha[k] == 0 && ('a' + k) != 'j')
					{
						table[i][j] = 'a' + k;
						alpha[k] = 1;
						break;
					}
				}
			}
		}
	}

	//for (int i = 0; i < 5; i++)
	//{
	//	for (int j = 0; j < 5; j++)
	//		cout << table[i][j] << " ";
	//	cout << endl;
	//}

	// 修改 plaintext
	// 倆倆分開，比到 len - 1
	string tempPlain;
	for (int i = 0; i < plaintext.length(); i += 2)
	{
		tempPlain += plaintext[i];
		if (plaintext[i] == plaintext[i + 1])
			tempPlain += "x";
		if (i == plaintext.length() - 1)
			continue;
		tempPlain += plaintext[i + 1];
	}
	// 如果尾巴只剩奇數
	if (tempPlain.length() % 2 == 1)
		tempPlain += "x";
	plaintext = tempPlain;

	string encryptedText;

	// 對照 table
	int row1, row2, col1, col2;
	for (int t = 0; t < plaintext.length(); t += 2)
	{
		string front(1, plaintext[t]);
		for (int i = 0; i < 5; i++)
		{
			for (int j = 0; j < 5; j++)
			{
				if (table[i][j] == front)
				{
					row1 = i;
					col1 = j;
					break;
				}

			}
		}
		string back(1, plaintext[t + 1]);
		for (int i = 0; i < 5; i++)
		{
			for (int j = 0; j < 5; j++)
			{
				if (table[i][j] == back)
				{
					row2 = i;
					col2 = j;
					break;
				}

			}
		}

		if (row1 == row2)
		{
			if (col1 != 4)
				encryptedText += table[row1][col1 + 1];
			else
				encryptedText += table[row1][0];
			if (col2 != 4)
				encryptedText += table[row2][col2 + 1];
			else
				encryptedText += table[row2][0];
		}

		else if (col1 == col2)
		{
			if (row1 != 4)
				encryptedText += table[row1 + 1][col1];
			else
				encryptedText += table[0][col1];
			if (row2 != 4)
				encryptedText += table[row2 + 1][col2];
			else
				encryptedText += table[0][col2];
		}

		else
		{
			encryptedText += table[row1][col2];
			encryptedText += table[row2][col1];
		}
	}

	//string encryptedText;
	return encryptedText;
}