#include <iostream>
#include <string>
using namespace std;

int main()
{
	string seg7[3][10] = {	{ " _ ","   "," _ "," _ ","   "," _ "," _ "," _ "," _ "," _ " } ,
							{ "| |","  |"," _|"," _|","|_|","|_ ","|_ ","  |","|_|","|_|" } ,
							{ "|_|","  |","|_ "," _|","  |"," _|","|_|","  |","|_|"," _|" } };
	string str;
	while (getline(cin,str))
	{
		for (int j = 0; j < 3; j++)
		{
			for (int i = 0; i < str.length(); i++)
			{
				if (str[i] >= '0' && str[i] <= '9')
				{
					cout << seg7[j][str[i] - '0'];
				}
			}
			cout << endl;
		}
	}
	return 0;
}