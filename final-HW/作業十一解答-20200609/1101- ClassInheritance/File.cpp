#include "File.h"

// ======================
// Assignment operator
// Invoke the base operator and then
// copy all variables from the right to left.
// ======================
File& File::operator =(const File& rightSide)
{
	// First invoke base operator
	Document::operator =(rightSide);
	// Simply copy the other string values
	pathname = rightSide.pathname;
	return *this;
}