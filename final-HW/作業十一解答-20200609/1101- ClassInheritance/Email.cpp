#include "Email.h"

// ======================
// Assignment operator
// Invoke the base operator and then
// copy all variables from the right to left.
// ======================
Email& Email::operator =(const Email& rightSide)
{
	// First invoke base operator
	Document::operator =(rightSide);
	// Simply copy the other string values
	sender = rightSide.sender;
	recipient = rightSide.recipient;
	title = rightSide.title;
	return *this;
}