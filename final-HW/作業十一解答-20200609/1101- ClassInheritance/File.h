#ifndef FILE_H
#define FILE_H

#include "Document.h"

// Define class for File, derive from Document
// For brevity, short one-line methods are defined here in the
// header.

class File : public Document
{
public:
	File() : Document()
	{
	}
	File(string body, string pathname) :
		Document(body), pathname(pathname)
	{
	}
	void setPathname(string s)
	{
		pathname = s;
	}
	string getPathname()
	{
		return pathname;
	}
	File& operator =(const File& rightSide);
private:
	string pathname;
};

#endif // !FILE_H
