#ifndef DOCUMENT_H
#define DOCUMENT_H

//document.cpp
//
//  This program introduces inheritance through a problem of
//  creating two types of Documents, email and files.  
//  Both inherit from Document with a "text" property that
//  we use to search for keywords of any subclass.

#include <iostream>
#include <string>

using namespace std;


// Base class for a Document.
// For brevity, short one-line methods are defined here in the
// header.

class Document
{
public:
	Document()
	{
		text = "";
	}
	Document(string text)
	{
		this->text = text;
	}
	string getText() const
	{
		return this->text;
	}
	Document& operator =(const Document& rightSide);
private:
	string text;
};

#endif // !DOCUMENT_H