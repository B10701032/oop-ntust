#include<iostream> 
using namespace std;
class NegativeDeposit {};
class InsufficientFunds {};
class Account
{
private:
	double balance;
public:
	Account()
	{
		balance = 0;
	}
	Account(double initialDeposit)
	{
		balance = initialDeposit;
	}
	double getBalance()
	{
		return balance;
	}

	double deposit(double amount)
	{
		if (amount > 0)
			balance += amount;
		else {
			throw NegativeDeposit();
		}
		return balance;
	}

	double withdraw(double amount)
	{
		if ((amount > balance) || (amount < 0)) {
			throw InsufficientFunds();
		}
		else
			balance -= amount;
		return balance;
	}
};