#include <iostream>
#include <string>
#include <set>
using namespace std;
string getinput(string keys, string input) {
	string s;
	int pos = input.find(keys);
	if (pos == string::npos || input.find(keys, pos + 1==string::npos)) {
		cout << "Incomplete Command.";
	}
	else {
		s = input.substr(pos + 1, input.find(keys, pos + 1)-1);
	}
	return s;
};
int main() {
	set<set<set<string>>> authorData;
	set<string> iterauthor;
	set<string> itertitle;
	set<string> iteredition;
	string input;
	string author;
	string title;
	string type;
	int edition;

	while (getline(cin, input)) {
		bool debug = true;
		int pos = input.find(" ");
		string command = input.substr(0, pos);
		if(debug) cout << "command: " << command << endl;
		if (command == "Insert") {
			title = getinput("\"", input.substr(command.length()+1));
			if (debug) cout << "title: " << title << endl;
			if (title.length() < 1) continue;
			author = getinput("\"", input.substr(command.length() + title.length() + 4));
			if (debug) cout << "author: " << author << endl;
			if (author.length() < 1) continue;
			edition = atoi(input.substr(command.length() + title.length() + author.length() + 7).c_str());
			if (debug) cout << "edition: " << edition << endl;
			if (edition < 1) continue;
		}
		else if (command == "Delete") {
			int pos2 = input.find(" ", pos);
			type = input.substr(pos + 1, pos2 - 1);
			if (debug) cout << "type: " << type << endl;

		}
		else if (command == "Find") {}
		else if (command == "Sort") {}
		else cout << "Unknown Command.\n";
		cin.ignore();
	}
}