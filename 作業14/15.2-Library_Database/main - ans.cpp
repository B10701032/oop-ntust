#include <iostream>
#include <string>
#include<vector>
using namespace std;

struct Datum {
	string title, author;
	vector<int> editions;
	Datum() {
		title = "", author = "";
		editions = vector<int>();
	}
	Datum(string author, string title, int edition) {
		this->author = author;
		this->title = title;
		editions = vector<int>();
		editions.push_back(edition);
	}
	Datum(string author, string title, vector<int> editions) {
		this->author = author;
		this->title = title;
		this->editions = editions;
	}
};

void printIncompleteCommand() {
	cout << "Incomplete Command." << endl;
}

void printUnknownCommand() {
	cout << "Unknown Command." << endl;
}

string getInput(string delim, string input) {
	string s = "";
	int pos = input.find(delim);
	if (pos == string::npos || input.find(delim, pos + 1) == string::npos) {
		printIncompleteCommand();
	}
	else {
		s = input.substr(pos + 1, input.find(delim, pos + 1) - 1);
	}

	return s;
}

int toInt(string s) {
	char* pointer;
	long val = strtol(s.c_str(), &pointer, 10);
	if (*pointer) {
		// not a number
		printUnknownCommand();
		return -1;
	}
	return val;
}

int main() {
	string input;
	vector<Datum> datums;
	bool debug = true;

	while (getline(cin, input)) {
		if (debug && input.length() < 1) break;
		if (input.find(" ") == string::npos) {
			if (input == "Insert" || input == "Delete" || input == "Sort" || input == "Find") printIncompleteCommand();
			else printUnknownCommand();
			continue;
		}
		string command = input.substr(0, input.find(" "));//找最前面的指令
		if (debug) cout << "command: \"" << command << "\"" << endl; 
		if (command == "Insert") {
			string title = getInput("\"", input.substr(command.length() + 1));//(",指令後面的input到結束)
			if (title.empty()) continue;
			if (debug) cout << "title: \"" << title << "\"" << endl;
			string author = getInput("\"", input.substr(command.length() + title.length() + 4));
			if (author.empty()) continue;
			if (debug) cout << "author: \"" << author << "\"" << endl;
			string editionString = input.substr(command.length() + title.length() + author.length() + 6);
			if (debug) cout << "editionString: \"" << editionString << "\"" << endl;
			if (editionString.empty()) {
				printIncompleteCommand();
				continue;
			}
			long edition = toInt(editionString);
			if (edition != -1) {
				// use converted
				bool inserted = false;
				if (datums.empty()
					|| datums[datums.size() - 1].author < author
					|| (datums[datums.size() - 1].author == author && datums[datums.size() - 1].title < title)) {
					inserted = true;
					datums.push_back(Datum(author, title, edition));
				}
				if (!inserted) {
					// insert sort by author
					for (int i = 0; i < datums.size() && !inserted; i++)
					{
						if (datums[i].author == author && datums[i].title == title) {
							if (datums[i].editions.empty()) {
								datums[i].editions.push_back(edition);
								inserted = true;
							}
							else {
								bool found = false;
								for (int j = 0; j < datums[i].editions.size() && !found; j++) if (datums[i].editions[j] == edition) found = true;
								if (!found) {
									datums[i].editions.push_back(edition);
									inserted = true;
								}
							}
						}
						else if (datums[i].author == author && datums[i].title > title
							|| datums[i].author > author) {
							datums.insert(datums.begin() + i, Datum(author, title, edition));
							inserted = true;
						}
					}
				}
				if (inserted) {
					cout << "Insert " << author << "'s " << title << ", Edition: " << edition << "." << endl;
				}
				else {
					cout << "Datum already exist." << endl;
				}
			}
		}
		else if (command == "Delete") {
			string type = getInput(" ", input.substr(command.length()));
			if (type.empty()) continue;
			if (debug) cout << "type: \"" << type << "\"" << endl;
			string title = getInput("\"", input.substr(command.length() + type.length() + 2));
			if (title.empty()) continue;
			if (debug) cout << "title: \"" << title << "\"" << endl;
			string author = getInput("\"", input.substr(command.length() + type.length() + title.length() + 5));
			if (author.empty()) continue;
			if (debug) cout << "author: \"" << author << "\"" << endl;

			if (type != "Edition" && type != "Book") printUnknownCommand();

			bool found = false;
			for (int i = 0; i < datums.size() && !found; i++)
			{
				if (datums[i].author == author && datums[i].title == title)
				{

					if (type == "Edition") {
						string editionString = input.substr(command.length() + type.length() + title.length() + author.length() + 8);
						if (debug) cout << "editionString: \"" << editionString << "\"" << endl;
						if (editionString.empty()) {
							printIncompleteCommand();
							continue;
						}
						long edition = toInt(editionString);
						if (edition != -1) {
							if (datums[i].editions.empty()) break;
							for (int j = 0; j < datums[i].editions.size() && !found; j++) {
								if (datums[i].editions[j] == edition) {
									found = true;
									datums[i].editions.erase(datums[i].editions.begin() + j);
									cout << "Delete " << author << "'s " << title << ", Edition: " << edition << "." << endl;
								}
							}
						}
					}
					else {
						if (datums[i].editions.empty()) break;
						while (!datums[i].editions.empty()) {
							found = true;
							cout << "Delete " << author << "'s " << title << "." << endl;
							datums[i].editions.erase(datums[i].editions.begin());
						}
					}

				}
				else if (datums[i].author > author || (datums[i].author == author && datums[i].title > title)) break;
			}
			if (!found) {
				cout << (type == "Edition" ? "Datum" : "Book") << " doesn't exist." << endl;
			}

		}
		else if (command == "Find") {
			string type = getInput(" ", input.substr(command.length()));
			if (type.empty()) continue;
			if (debug) cout << "type: \"" << type << "\"" << endl;
			if (type == "Author") {
				string author = getInput("\"", input.substr(command.length() + type.length() + 2));
				if (author.empty()) continue;
				if (debug) cout << "author: \"" << author << "\"" << endl;
				bool found = false;
				string s = "";
				for (int i = 0; i < datums.size(); i++)
				{
					if (datums[i].author == author)
					{
						if (s.empty()) {
							s += author + "\'s Books: " + datums[i].title;
						}
						else {
							s += ", " + datums[i].title;
						}
					}
				}
				if (s.empty()) {
					cout << "No book found.";
				}
				else {
					cout << s;
				}
				cout << endl;
			}
			else if (type == "Book") {
				string title = getInput("\"", input.substr(command.length() + type.length() + 2));
				if (title.empty()) continue;
				if (debug) cout << "title: \"" << title << "\"" << endl;
				string author = getInput("\"", input.substr(command.length() + type.length() + title.length() + 5));
				if (author.empty()) continue;
				if (debug) cout << "author: \"" << author << "\"" << endl;
				bool found = false;
				for (int i = 0; i < datums.size(); i++)
				{
					if (datums[i].author == author && datums[i].title == title) {
						if (!datums[i].editions.empty())
						{
							found = true;
							// sort
							int* arr = new int[datums[i].editions.size()];
							for (int j = 0; j < datums[i].editions.size(); j++)
							{
								arr[j] = datums[i].editions[j];
								for (int k = 0; k < j + 1; k++)
								{
									if (arr[k] > arr[j]) {
										arr[k] = arr[k] + arr[j];
										arr[j] = arr[k] - arr[j];
										arr[k] = arr[k] - arr[j];
									}
								}
							}
							cout << "Title: " << title << "\tAuthor: " << author << "\tEdition: ";
							for (int j = 0; j < datums[i].editions.size(); j++)
							{
								if (j == 0) cout << arr[j];
								else cout << ", " << arr[j];
							}
							cout << endl;
						}
					}
				}
				if (!found) {
					cout << "No book found." << endl;
				}
			}
			else {
				printUnknownCommand();
				continue;
			}
		}
		else if (command == "Sort") {
			if (debug) cout << "datums size: " << datums.size() << endl;
			if (input == "Sort by Author") {
				// default
				for (int i = 0; i < datums.size(); i++)
				{
					if (!datums[i].editions.empty())
					{
						cout << "Title: " << datums[i].title << "\tAuthor: " << datums[i].author << "\tEdition: ";
						// sort
						int* arr = new int[datums[i].editions.size()];
						for (int j = 0; j < datums[i].editions.size(); j++)
						{
							arr[j] = datums[i].editions[j];
							for (int k = 0; k < j + 1; k++)
							{
								if (arr[k] > arr[j]) {
									arr[k] = arr[k] + arr[j];
									arr[j] = arr[k] - arr[j];
									arr[k] = arr[k] - arr[j];
								}
							}
						}
						for (int j = 0; j < datums[i].editions.size(); j++)
						{
							if (j == 0) cout << arr[j];
							else cout << ", " << arr[j];
						}
						cout << endl;
					}
				}
			}
			else if (input == "Sort by Title") {
				// need to sort
				vector<Datum> v;
				for (int i = 0; i < datums.size(); i++)
				{
					if (!datums[i].editions.empty())
					{
						// sort
						vector<int> arr;
						for (int j = 0; j < datums[i].editions.size(); j++)
						{
							arr.push_back(datums[i].editions[j]);
							for (int k = 0; k < j + 1; k++)
							{
								if (arr[k] > arr[j]) {
									arr[k] = arr[k] + arr[j];
									arr[j] = arr[k] - arr[j];
									arr[k] = arr[k] - arr[j];
								}
							}
						}
						if (v.empty() ||
							v[v.size() - 1].title < datums[i].title
							|| (v[v.size() - 1].title == datums[i].title && v[v.size() - 1].author < datums[i].author)
							) {
							v.push_back(Datum(datums[i].author, datums[i].title, arr));
						}
						else {
							for (int j = 0; j < v.size(); j++)
							{
								if (debug) cout << datums[i].title << ", " << datums[i].author << endl;
								if (debug) cout << v[j].title << ", " << v[j].author << endl;
								if (datums[i].title < v[j].title
									|| (datums[i].title == v[j].title && datums[i].author < v[j].author)) {
									v.insert(v.begin() + j, Datum(datums[i].author, datums[i].title, arr));
									break;
								}
							}
						}
					}
				}
				for (int i = 0; i < v.size(); i++)
				{
					cout << "Title: " << v[i].title << "\tAuthor: " << v[i].author << "\tEdition: ";
					for (int j = 0; j < v[i].editions.size(); j++)
					{
						if (j == 0) cout << v[i].editions[j];
						else cout << ", " << v[i].editions[j];
					}
					cout << endl;
				}
			}
			else {
				printUnknownCommand();
				continue;
			}
		}
		else {
			printUnknownCommand();
		}
	}
	return 0;
}