#include <iostream>
#include <string>
using namespace std;

template<typename T>
void ItrBinarySearch(const T a[], int first, int last, T key, bool& found, int& location) {
	int i = first;
	int j = last;
	int mid;
	while (i <= j) {
		mid = (i + j) / 2;
		if (a[mid] == key) {
			found = true;
			location = mid;
			return;
		}
		else if (a[mid] < key) {
			i = mid + 1;
		}
		else {
			j = mid - 1;
		}
	}
	found = false;
	location = -1;
	return;
}


template<typename T>
void RecBinarySearch(const T a[], int first, int last, T key, bool& found, int& location) {
	if (first > last) {
		found = false;
		location = -1;
		return;
	}
	int mid = (first + last) / 2;
	if (a[mid] == key) {
		found = true;
		location = mid;
		return;
	}
	else if (a[mid] < key) {
		RecBinarySearch(a, mid + 1, last, key, found, location);
	}
	else {
		RecBinarySearch(a, first, mid - 1, key, found, location);
	}
	return;
}
