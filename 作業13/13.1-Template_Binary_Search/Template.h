template <typename T>
void ItrBinarySearch(const T arr[], int firstidx, int endidx, const T keyString, bool& found, int& location) {
	int i = firstidx;
	int j = endidx;
	found=false;
	while (i <= j) {
		int mid = (i + j) / 2;
		if (arr[mid] == keyString) {
			found = true;
			location = mid;
			return;
		}
		else if (arr[mid] > keyString) {
			j = mid - 1;
		}
		else if (arr[mid] < keyString) {
			i = mid + 1;
		}
	}
	found = false;
	location = -1;
	return;
}
template <typename T>
void RecBinarySearch(const T arr[], int start, int end, const T keyString, bool& found, int& location) {
	if (start > end) {
		found = false;
		location = -1;
		return;
	}
	int mid = (start + end) / 2;
	if (arr[mid] == keyString) {
		found = true;
		location = mid;
	}
	else if (arr[mid] > keyString) {
		RecBinarySearch(arr,start,mid -1,keyString,found,location);
	}
	else if (arr[mid] < keyString) {
		RecBinarySearch(arr,mid+1,end,keyString,found,location);
	}
	return;
}