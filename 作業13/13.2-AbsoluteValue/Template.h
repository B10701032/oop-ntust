#pragma once
#include <cmath>
template <typename T>
double absoluteValue(const T& lhs, const T& rhs) {
	double result = lhs - rhs;
	if (result <= 0) return -result;
	else return result;
}