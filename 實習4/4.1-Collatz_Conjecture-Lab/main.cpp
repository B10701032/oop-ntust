#include <iostream>

using namespace std;

int main() {
    int number;
    int number2;
    int max = 0;
    int longestCycle = 0;
    int begin = 0;
    int i = 0;
    while (cin >> number >> number2) {
        cout << number << " " << number2 << " ";
        max = 0;
        if (number > number2) {
            int tmp = number;
            number = number2;
            number2 = tmp;
        }
        begin = number;
        for (begin; begin <= number2; begin++) {
            int n = begin;
            i = 0;
            while (n != 1) {
                if (n % 2 == 0) {
                    n = n / 2;
                }
                else {
                    n = n * 3 + 1;
                }
                i++;
                //cout << n << " ";
            }
            //cout <<endl << "begin:" << begin << "cycle = " << i << endl;
            if (i > max) {
                max = i;
                longestCycle = begin;
            }
        }
        cout  << max+1 << endl;
    }
}