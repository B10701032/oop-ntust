class BankAccount {
private:
	int balance;
	static int total;
public:
	BankAccount() {
		BankAccount::balance = 0;
		BankAccount::total += 0;
	};
	BankAccount(int input) {
		BankAccount::balance = input;
		BankAccount::total += input;
	}
	void withdraw(int output) {
		BankAccount::balance -= output;
		BankAccount::total -= output;
	};
	void save(int input) {
		BankAccount::balance += input;
		BankAccount::total += input;
	};
	int getBalance() {
		return BankAccount::balance;
	};
	static int getAllMoneyInBank() {
		return BankAccount::total;
	}
};
int BankAccount::total = 0;


