﻿#include <iostream>
using namespace std;
const int CAPACITY = 10001;
struct BigInt {
    string bigInt;
    BigInt();
    BigInt(string n);
    int digits[CAPACITY];
    int digitSize;
    void print();
};
void BigInt::print() {
    // cout << "print ";
    // cout.std::ios_base::imbue(locale("")); 
    for (int i = digitSize - 1; i >= 0; i--) {
        cout << digits[i];
    }
    cout << endl;
}
BigInt Add(const BigInt& lhs, const BigInt& rhs) {
    BigInt result = rhs;
    for (int i = 0; i < CAPACITY; i++)
    {
        result.digits[i] += lhs.digits[i];
        // reached the end, so break
        if (i >= result.digitSize && i >= lhs.digitSize && result.digits[i] == 0) break;
        else if (result.digits[i] >= 10) {
            if (i <= CAPACITY - 2) {
                result.digits[i + 1] += 1;
                result.digits[i] -= 10;
            }
            // max value
            else if (i == CAPACITY - 1) {
                result.digits[i] = 9;
            }
        }
    }
    // update digit size
    for (int i = CAPACITY - 1; i >= 0; i--) {
        if (result.digits[i] != 0) {
            result.digitSize = i + 1;
            break;
        }
    }
    // cout << "plus digitSize " << digitSize << endl;
    return result;
}
BigInt::BigInt(string numberString) {
    bigInt = numberString;
    // remove whitespaces
    while (numberString.find(' ') != string::npos)
        numberString.erase(numberString.find(' '), 1);
    // cout << "number string " << numberString << endl;
    for (int i = 0; i < CAPACITY; i++) digits[i] = 0;
	 // cout << "number string again " << numberString << " " << positive << endl;

    digitSize = numberString.length();
    // cout << "digit size " << digitSize << endl;
    for (int i = 0; i < digitSize; i++) {
        // cout << "i " << i << " nmstring " << numberString[digitSize - i - 1] << endl;
        digits[i] = numberString[digitSize - i - 1] - '0';
        // cout << "digits " << i << ": " << digits[i] << endl; 
    }

    // cout << "digitSize " << digitSize;
}

int main() {
	int Cnt = 0;
	while (cin >> Cnt) {
        string a;
        string b;
        
        for (int i = 0; i < Cnt; i++) {
            cin >> a;
            cin >> b;
            bool skip = false;
            for (int i = 0; i < a.length() ; i++) {
                if (!isdigit(a[i]) or !isdigit(b[i])) {
                    cout << "Not a valid number, please try again.";
                    skip = true;
                    break;
                }
            }

            if (skip == true) continue;

            BigInt lhs(a);
            BigInt rhs(b);
            //cout << "lhs: " << lhs.bigInt << " rhs: " << rhs.bigInt << endl;
            BigInt result = Add(lhs, rhs);
            //cout << result.bigInt << endl;
            result.print();
        }
	}
}