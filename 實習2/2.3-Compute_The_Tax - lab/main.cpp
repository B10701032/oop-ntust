#include <iostream>
#include <iomanip>
using namespace std;

int main() {
    float money;
    float tax = 0;
    while (cin >> money) {
        if (money <= 750) tax = money * (0.01);
        if (money > 750 and money <= 2250) tax = 7.50 + (money - 750) * (0.02);
        if (money > 2250 and money <= 3750)  tax = 37.5 + (money - 2250) * (0.03);
        if (money > 3750 and money <= 5250) tax = 82.50 + (money - 3750) * (0.04);
        if (money > 5250 and money <= 7000) tax = 142.50 + (money - 5250) * (0.05);
        if (money > 7000) tax = 230.00 + (money - 7000) * (0.06);
        cout << fixed << setprecision(2) << tax << endl;
    }
    return 0;
}