#include <iostream>
#include <vector>
using namespace std;

int main() {
    int Cnt = 0;
    char nameList[100][100];
    int NameCnt[100] = { 0 };
    char n1List[100][100];
    int n1Cnt[100];
    char n2List[100][100];
    int n2Cnt[100];
    
    while (cin >> Cnt) {
        int MaxName = 0;
        int MaxN1 = 0;
        int MaxN2 = 0;
        for (int i = 0; i < Cnt; i++) {
            cin >> nameList[i];
            for (int j = 0; nameList[i][j] != '\0'; j++) {
                NameCnt[i] = j;
                if (MaxName < j) {
                    MaxName = j;
                }
            }
            cin >> n1List[i];
            for (int j = 0; n1List[i][j] != '\0'; j++) {
                n1Cnt[i] = j;
                if (MaxN1 < j) {
                    MaxN1 = j;
                }
            }
            cin >> n2List[i];
            for (int j = 0; n2List[i][j] != '\0'; j++) {
                n2Cnt[i] = j;
                if (MaxN2 < j) {
                    MaxN2 = j;
                }
            }
        }
        for (int i = 0; i < Cnt; i++) {
            if (NameCnt[i] < MaxName) {
                for (int j = NameCnt[i]; j != MaxName ; j++) {
                    cout << " ";
                    }
                cout << nameList[i] << "|  ";
            } 
            else cout << nameList[i] << "|  ";
            
            if (n1Cnt[i] < MaxN1) {
                for (int j = n1Cnt[i]; j != MaxN1; j++) {
                    cout << " ";
                }
                cout << n1List[i] << "|  ";
            }
            else cout << n1List[i] << "|  ";

            if (n2Cnt[i] < MaxN2) {
                for (int j = n2Cnt[i]; j != MaxN2; j++) {
                    cout << " ";
                }
                cout << n2List[i] << endl;
            }
            else cout << n2List[i] << endl;
        }
    }
}