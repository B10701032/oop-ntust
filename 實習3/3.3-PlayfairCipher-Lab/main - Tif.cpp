#include <iostream>
#include <string>
using namespace std;

int main() {
    int totalAlphabet = 'z' - 'a', totalColumn = 5, totalRow = 5;
    //總共25個字母
    //共5個row,5個column
    int* alphabetKeys = new int[totalAlphabet + 1]{ -1 },
        // example a -> 2, b -> 1, c -> 0 //asiic 
        * keyTable = new int[totalColumn * totalRow]{ -1 },
        //if key = cba then keyTable[0] == 50 'c' keyTable[1] == 49 'b' keyTable[2] == 48 'a'
        inputs[2] = { -1 }, columns[2] = { -1 }, rows[2] = { -1 };
    //input 放每2個一組的字元//columns去記inputs[0]和inputs[1]的行位置
    int i, j, idx;
   // i 跑 keywords 
    string input, key, encrypted;

    while (getline(cin, input)) {
        if (input.length() < 1) continue;
        do {
            getline(cin, key);
        } while (key.length() < 1);

        // reset alphabetkeys and totalalphabet
        for (int j = 0; j <= totalAlphabet; j++) {
            alphabetKeys[j] = -1;
        }
        for (int j = 0; j < totalColumn * totalRow; j++) {
            keyTable[j] = -1;
        }


        //cout << "input: " << input << endl;
        //cout << "key: " << key << endl;

        // generate key table
        i = 0;
        //i = keytable 的 idx
        for (int j = 0; j < key.length(); j++) {
            if (key[j] == 'j') key[j] = 'i';
            idx = key[j] - 'a';
            //idx 是第幾個字母 a = 0 , b = 1 以此類推
            // if it is uninitialized
            if (alphabetKeys[idx] < 0) {
                //若小於0，代表該字母還沒存在過，所以在keytable的位置為-1
                alphabetKeys[idx] = i;
                //所以更新該字母應該要在keytable的i的位置
                keyTable[i] = key[j];
                //在keyTable[i]的字母是key[j]但因為儲存的是int 所以是應對asiic值
                //a->48;
                //b->49;
                i++;
            }
        }

        for (int j = 0; j <= totalAlphabet; j++) {
            //j 去 run 所有字母 a -> 0 , b -> 1, c -> 2 
            if (j == 'j' - 'a') continue;
            if (alphabetKeys[j] < 0) {
                alphabetKeys[j] = i;
                keyTable[i] = j + 'a';
                i++;
            }
        }

        // cout alphabet keys
        /*for (int j = 0; j <= totalAlphabet; j++) {
         cout << "key " << (char)(j + 'a') << ": " << alphabetKeys[j] << endl;
        }
        for (int j = 0; j < totalColumn * totalRow; j++) {
         cout << "keytable " << j << ": " << keyTable[j] << endl;
        }*/


        encrypted = "";
        // encrypt input
        for (j = 0; j < input.length();) {
            inputs[0] = input[j];
            j++;
            // if on the end or the same
            if (j == input.length() || input[j] == input[j - 1]) {
                inputs[1] = 'x';
            }
            else {
                inputs[1] = input[j];
                j++;
            }

            for (int k = 0; k < 2; k++) {
                idx = inputs[k] - 'a';
                rows[k] = alphabetKeys[idx] / totalRow;
                columns[k] = alphabetKeys[idx] % totalColumn;
            }

            // same row
            if (rows[0] == rows[1]) {
                encrypted += keyTable[rows[0] * totalRow + ((columns[0] + 1) % totalColumn)];
                encrypted += keyTable[rows[1] * totalRow + ((columns[1] + 1) % totalColumn)];
            }

            // same column
            else if (columns[0] == columns[1]) {
                encrypted += keyTable[((rows[0] + 1) % totalRow) * totalRow + columns[0]];
                encrypted += keyTable[((rows[1] + 1) % totalRow) * totalRow + columns[1]];
            }
            // rectangle
            else {
                // just swap the columns
                // thanks fulgen!
                encrypted += keyTable[rows[0] * totalRow + columns[1]];
                encrypted += keyTable[rows[1] * totalRow + columns[0]];
            }
        }
        cout << encrypted << endl;
    }


    return 0;
}