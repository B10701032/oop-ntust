#include <iostream>
#include <string>
using namespace std;
int main() {
	int alphabetIndex[26];
	int alphaCnt = 'z' - 'a';
	int keyboard[26];
	int input[2],rowidx[2],columnidx[2];
	int rowCnt = 5;
	int columnCnt = 5;
	string keys, compareWord, ans;
	int i, idx;
	while (getline(cin, compareWord)) {
		if (compareWord.length() < 1) continue;
		do {
			getline(cin, keys);
		} while (keys.length() < 1);
		for (int i = 0; i < 26; i++) {
			alphabetIndex[i] = -1;
			keyboard[i] = -1;
		}
		i = 0;
		for (int j = 0; j < keys.length(); j++) {
			if (keys[j] == 'j') keys[j] = 'i';
			idx = keys[j] - 'a';
			if (alphabetIndex[idx] < 0) {
				alphabetIndex[idx] = i;
				keyboard[i] = keys[j];
				i++;
			}
		}
		for (int j = 0; j <= alphaCnt; j++) {
			if (j == 'j' - 'a') continue;
			if (alphabetIndex[j] < 0) {
				alphabetIndex[j] = i;
				keyboard[i] = 'a' + j;
				i++;
			}
		}
		for (int i = 0; i < 26; i++) {
			std::cout << static_cast<char>(keyboard[i]) << endl;
		}
		ans = "";
		for (int j = 0; j < compareWord.length(); j) {
			input[0] = compareWord[j];
			j++;
			if (input[0] == compareWord[j] || j == compareWord.length()) {
				input[1] = 'x';
			}
			else {
				input[1] = compareWord[j];
				j++;
			}
			for (int k = 0; k < 2; k++) {
				idx = alphabetIndex[input[k]-'a'];
				rowidx[k] = idx / rowCnt;
				columnidx[k] = idx % columnCnt;
			}
			if (rowidx[0] == rowidx[1]) {
				ans += keyboard[rowidx[0] * rowCnt + (columnidx[0] + 1) % columnCnt];
				ans += keyboard[rowidx[1] * rowCnt + (columnidx[1] + 1) % columnCnt];
			}
			else if (columnidx[0] == columnidx[1]) {
				ans += keyboard[((rowidx[0] + 1) % rowCnt)*rowCnt + columnidx[0]];
				ans += keyboard[((rowidx[1] + 1) % rowCnt)*rowCnt + columnidx[1]];
			}
			else {
				ans += keyboard[rowidx[0] * rowCnt + columnidx[1]];
				ans += keyboard[rowidx[1] * rowCnt + columnidx[0]];
			}
		}
		cout << ans << endl;
		compareWord.clear();
		keys.clear();
	}
}
