#include <iostream>
#include <string>
#include <sstream>
#include <vector>
using namespace std;

int main() {
	string text;
	string num;
	vector<int> order;
	int tmp;
	int Cnt;
	char ans[20][20];
	stringstream s("");

	while (getline(cin, text)) {
		int column = 0;
		Cnt = text.length();
		getline(cin, num);
		s << num;
		while (s >> tmp) {
			order.push_back(tmp);
		}
		int n = 0;
		bool skip(false);
		if (Cnt % order.size() != 0) column = Cnt / order.size() + 1;
		else column = Cnt / order.size();
		for (int i = 0; i < order.size(); i++) {
			if (skip) break;
			for (int j = 0; j < column; j++) {
				if (text[order[i] + j * order.size()] != NULL) {
					if (text[order[i] + j * order.size()] == ' ') cout << "_";
					else if (text[order[i] + j * order.size()] > 32 && text[order[i] + j * order.size()] < 127)
						cout << text[order[i] + j * order.size()];
					n++;
					if (n == Cnt) {
						skip = true;
						break;
					}
				}
			}
		}
		cout << endl;
		order.clear();
		s.clear();
		s.str("");
		cin.ignore(0,EOF);
	}
}
