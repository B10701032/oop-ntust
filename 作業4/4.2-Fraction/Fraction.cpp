#include "Fraction.h"

void Fraction::setNumerator(int nu)
{
	Fraction::numerator = nu;
}

void Fraction::setDenominator(int de)
{
	Fraction::denominator = de;
}

void Fraction::getDouble()
{
	double a;
	a = static_cast<double>(numerator) / static_cast<double>(denominator);
	if (numerator % denominator == 0) cout << static_cast<int>(a) << endl;
	else cout << fixed << setprecision(6) << a << endl;
}

void Fraction::outputReducedFraction()
{
	int Max;
	int HCF;
	if (Fraction::denominator > Fraction::numerator)    Max = Fraction::denominator;
	else Max = Fraction::numerator;
	if (Fraction::numerator%Fraction::denominator!=0) {
		for (int i = 1; i < Max; i++) {
			if (Fraction::denominator % i == 0 && Fraction::numerator % i == 0) HCF = i;
		}
		cout << Fraction::numerator / HCF << "/" << Fraction::denominator / HCF << endl;
	}
	else cout << Fraction::numerator / Fraction::denominator << endl;
}



