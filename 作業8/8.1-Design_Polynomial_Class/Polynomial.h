#include <string>
#include <vector>
#include <math.h>
using namespace std;
class Polynomial {
private:

public:
	vector<double> coefficient;
	double* co;
	int Size;
	Polynomial() {
		coefficient.insert(coefficient.begin(),0);
		Size = 1;
		co = new double[Size] {0};
	};
	Polynomial(double po[], int n) {
		for (int i = 0; i < n; i++) {
			this->coefficient.insert(coefficient.begin(), po[i]);
		}
		this->Size = coefficient.size();
		getArray(co , coefficient);
	};
	int mySize() {
		return Size;
	}

	Polynomial& operator=(const Polynomial& poly) {
		delete[] this->co;
		this->coefficient.clear();
		for (int i = 0; i < poly.coefficient.size(); i++) {
			this->coefficient[i] = poly.coefficient[i];
		}
		getArray(this->co , coefficient);
		return *this;
	};
	double& operator[](int n) {
		return co[coefficient.size()-n-1];
	};
	void getArray(double* co, vector<double> poly) {
		this->co = new double[poly.size()];
		for (int i = 0; i < Size; i++) {
			this->co[i] = poly[i];
		}
	}
	friend Polynomial operator+(const Polynomial& lhs,const Polynomial& rhs) {
		int max , min;
		if (lhs.coefficient.size() < rhs.coefficient.size()) { 
			min = lhs.coefficient.size();
			max = rhs.coefficient.size();
		}

		else {
			min = rhs.coefficient.size();
			max = lhs.coefficient.size();
		}
		double* co;
		int i(0);
		co = new double[rhs.coefficient.size()];
		for (i = 0; i < min; i++) {
			co[i] = rhs.coefficient[i] + lhs.coefficient[i];
		}
		for (i; i < max; i++) {
			if (min == lhs.coefficient.size()) {
				co[i] = rhs.coefficient[i];
			}
			else {
				co[i] = lhs.coefficient[i];
			}
		}
		return Polynomial(co, i);
	};
	friend Polynomial operator-(const Polynomial& lhs, const Polynomial& rhs) {
		int max, min;
		if (lhs.coefficient.size() < rhs.coefficient.size()) {
			min = lhs.coefficient.size();
			max = rhs.coefficient.size();
		}

		else {
			min = rhs.coefficient.size();
			max = lhs.coefficient.size();
			double* co;
			int i(0);
			co = new double[rhs.coefficient.size()];
			for (i = 0; i < min; i++) {
				co[i] = rhs.coefficient[i] - lhs.coefficient[i];
			}
			for (i; i < max; i++) {
				if (min == lhs.coefficient.size()) {
					co[i] = rhs.coefficient[i];
				}
				else {
					co[i] = -lhs.coefficient[i];
				}
			}
			return Polynomial(co, i);
		}
	}
	Polynomial operator*(Polynomial P) {
		for (int i = 0; i < P.coefficient.size(); i++) {
			coefficient[i] += P.coefficient[i];
		}
		//getArray();
		return Polynomial(co, mySize());
	};

};
double evaluate(const Polynomial& poly, const double& var) {
	double res(0);
	for (int i = 0; i < poly.coefficient.size(); i++) {
		res += poly.coefficient[i] * pow(2, poly.coefficient.size() - i-1);
	}
	return res;
}
