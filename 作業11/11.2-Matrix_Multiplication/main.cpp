#include <iostream>
using namespace std;
int add(int n, int  m, int matrixA[][100], int matrixB[][100],int column) {
    int acc = 0;
    for (int i = 0; i < column; i++) {
        acc += matrixA[n][i] * matrixB[i][m];
    }
    return acc;
};
int main() {
    int Arow;
    int Brow;
    int Acolumn;
    int Bcolumn;
    int matrixA[100][100]{ 0 };
    int matrixB[100][100]{ 0 };
    int finalmatrix[100][100]{ 0 };
    while (cin >> Arow >> Acolumn >> Brow >> Bcolumn) {
        if (Acolumn != Brow) cout << "Error" << endl;
        else {
            for (int i = 0; i < Arow; i++) {
                for (int j = 0; j < Acolumn; j++) {
                    cin >> matrixA[i][j];
                }
            }
            for (int i = 0; i < Brow; i++) {
                for (int j = 0; j < Bcolumn; j++) {
                    cin >> matrixB[i][j];
                }
            }
            for (int i = 0; i < Arow; i++) {
                for (int j = 0; j < Bcolumn; j++) {
                    cout << add(i, j, matrixA, matrixB, Acolumn) << " ";
                }
                cout << endl;
            }
        }
    }
}