#pragma once
#include "Document.h"
class File : public Document{
private:
	string pathname;
public:
	File() :pathname("") {};
	File(string text, string pathname) {
		setText(text);
		this->pathname = pathname;
	}
	File& operator=(const File& rhs) {
		setText(((Document)(rhs)).getText());
		this->pathname = rhs.pathname;
		return *this;
	}
	string getPathname() {
		return pathname;
	}
	void setPathname(string pathname) {
		this->pathname = pathname;
	}
};