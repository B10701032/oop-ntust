#pragma once

#include <iostream>
using namespace std;
class VecNf {
public:
	float* dest;
	int dimension;
	VecNf() {
		//cout << "dest ";
		this->dimension = 1;
		this->dest = new float[dimension] {0};
		//cout << dest[0] << endl;
	}
	VecNf(float* de, int d) {
		this->dimension = d;
		this->dest = new float[d];
		//cout << "dest = ";
		for (int i = 0; i < d; i++) {
			this->dest[i] = de[i];
			//cout << de[i] << " ";
		}
		//cout << endl;
	}
	VecNf(const VecNf& rhs) {
		this->dest = new float[rhs.dimension];
		this->dimension = rhs.dimension;
		for (int i = 0; i < this->dimension; i++) {
			this->dest[i] = rhs.dest[i];
		}
	}
	int Size() {
		return this->dimension;
	}
	VecNf& operator=(const VecNf& rhs) {
		cout << "ASSIGNMENT!!!" << endl;
		if (this == &rhs) {
			return (*this);
		}
		delete[] this->dest;
		this->dimension = rhs.dimension;
		this->dest = new float[this->dimension];
		for (int i = 0; i < this->dimension; i++) {
			this->dest[i] = rhs.dest[i];
		}
		return (*this);
	}
	float& operator[](int i) {
		return this->dest[i];
	}
	friend VecNf operator+(const VecNf& rhs, const VecNf& lhs) {
		if (rhs.dimension != lhs.dimension) {
			cout << "dimensions inconsistent" << endl;
			return VecNf();

		}
		else {
			float* dest;
			dest = new float[rhs.dimension]{ 0 };
			for (int i = 0; i < rhs.dimension; i++) {
				dest[i] = (rhs.dest[i] + lhs.dest[i]);
			}
			return VecNf(dest, rhs.dimension);
		}
	}
	friend VecNf operator-(const VecNf& rhs, const VecNf& lhs) {
		if (rhs.dimension != lhs.dimension) {
			cout << "dimensions inconsistent" << endl;
			return VecNf();

		}
		else {
			float* dest;
			dest = new float[rhs.dimension];
			for (int i = 0; i < rhs.dimension; i++) {
				dest[i] = rhs.dest[i] - lhs.dest[i];
			}
			return VecNf(dest, rhs.dimension);
		}
	}
	float operator*(const VecNf& rhs) {
		float value(0);
		if (rhs.dimension != dimension) {
			cout << "dimensions inconsistent" << endl;
			return value;
		}
		else {
			for (int i = 0; i < dimension; i++) {
				value += dest[i] * rhs.dest[i];
			}
			return value;
		}
	}
	friend VecNf operator*(const float n, const VecNf& rhs) {
		float* dest;
		dest = new float[rhs.dimension];
		for (int i = 0; i < rhs.dimension; i++) {
			dest[i] = rhs.dest[i]*n;
		}
		return VecNf(dest, rhs.dimension);
	}
};

//#include <iostream>
//using namespace std;
//
//class VecNf {
//private:
//	float *values;
//	int dim;
//public:
//	VecNf();
//	VecNf(float*, int);
//	VecNf(const VecNf&);
//	int Size();
//	VecNf& operator=(const VecNf&);
//	float& operator[](int);
//	VecNf operator+(const VecNf&);
//	friend VecNf operator-(const VecNf&, const VecNf&);
//	friend VecNf operator*(const VecNf&, const VecNf&);
//	friend ostream& operator<<(ostream&, const VecNf&);
//};