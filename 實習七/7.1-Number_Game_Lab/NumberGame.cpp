#include "NumberGame.h"
void NumberGame::ProcessInput() {
	string s = to_string(num);
	this->digits.clear();
	for (int i = 0; i < s.length(); i++)
	{
		int n = atoi(s.substr(i, 1).c_str());
		int size = this->digits.size();
		this->digits.push_back(n);
		for (int j = 0; j < size; j++)
		{
			this->digits.push_back(this->digits[j] * n);
		}
	}
}
void NumberGame::SetFileName(string input) {
	this->input = input;
}
void NumberGame::LoadNumberList() {
	string s;
	ifstream f(this->input);
	/*for (int j = 0; j < this->digits.size(); j++)
	{
		cout << this->digits[j] << " ";
	} cout << endl;*/
	if (f.is_open()) {
		this->valid_nums.clear();
		while (getline(f, s)) {
			int n = atoi(s.c_str());
			for (int i = 0; i < this->digits.size(); i++)
			{
				if (this->digits[i] == n) {
					this->valid_nums.push_back(n);
					break;
				}
			}
		}
		f.close();
	}
}
void NumberGame::PrintAllValid() {
	for (int i = 0; i < this->valid_nums.size(); i++)
	{
		cout << this->valid_nums[i] << endl;
	}
}
void NumberGame::Reset() {
	this->input = "";
	this->num = 0;
	this->digits.clear();
	this->valid_nums.clear();
}