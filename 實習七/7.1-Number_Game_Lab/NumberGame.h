#include <iostream>
#include <fstream>
#include <string>
#include <vector>

using namespace std;
class NumberGame {
public:
	string input;
	int num;
	vector<int> digits;
	vector<int> valid_nums;
	void SetInput(int n) {
		num = n;
	};
	void ProcessInput();
	void SetFileName(string input);
	void LoadNumberList();
	void PrintAllValid();
	void Reset();

}; 
