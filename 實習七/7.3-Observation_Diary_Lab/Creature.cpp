#include "Creature.h"

Creature::Creature(string name) {
	this->name = name;
}
Creature::Creature(string name, const Creature& c) {
	this->name = name;
	for (int i = 0; i < c.parts.size(); i++)
	{
		this->parts.push_back(c.parts[i]);
		this->values.push_back(c.values[i]);
	}
}
Creature& Creature::operator[](string part) {
	for (int i = 0; i < this->parts.size(); i++)
	{
		if (this->parts[i] == part) {
			this->idx = i;
			return (*this);
		}
	}
	this->parts.push_back(part);
	this->idx = this->parts.size() - 1;
	this->values.push_back(0);

	return (*this);
}
Creature& Creature::operator=(int value) {
	if (this->idx >= 0) {
		this->addToLogs(value);
		this->values[this->idx] = value;
	}
	this->idx = -1;
	return (*this);
}
Creature& Creature::operator+=(int value) {
	if (this->idx >= 0) {
		this->addToLogs(value);
		this->values[this->idx] += value;
	}
	this->idx = -1;
	return (*this);
}
Creature& Creature::operator-=(int value) {
	if (this->idx >= 0) {
		this->addToLogs(value);
		this->values[this->idx] -= value;
	}
	this->idx = -1;
	return (*this);
}
void Creature::PrintStatus() {
	cout << this->name.c_str() << "'s status:" << endl;
	for (int i = 0; i < this->parts.size(); i++)
	{
		if (this->values[i] > 0) {
			cout << this->parts[i].c_str() << " * " << this->values[i] << endl;
		}
	}
	cout << endl;
}
void Creature::PrintLog() {
	cout << this->name.c_str() << "'s Log:" << endl;
	int j = 0;
	for (;j < Diary::currentDate.size(); j++)
	{
		if ("Day " + Diary::currentDate[j] == this->logs[0]) break;
	}
	for (int i = 0; i < this->logs.size(); i++)
	{
		if (this->logs[i].find("Day") != string::npos) {
			for (; j < Diary::currentDate.size(); j++)
			{
				if ("Day " + Diary::currentDate[j] == this->logs[i]) {
					j++;
					break;
				}
				else {
					cout << "Day " + Diary::currentDate[j] << endl;
				}
			}
		}
		cout << this->logs[i] << endl;
	}
	for (; j < Diary::currentDate.size(); j++)
	{
		cout << "Day " + Diary::currentDate[j] << endl;
	}
	cout << endl;
}
void Creature::addToLogs(int value) {
	if (this->lastDate != Diary::currentDate[Diary::currentDate.size()-1]) {
		this->lastDate = Diary::currentDate[Diary::currentDate.size() - 1];
		this->logs.push_back("Day " + this->lastDate);
	}
	string s = this->name + "'s " + this->parts[this->idx];
	if (this->values[this->idx] == 0 && value > 0) {
		s.append(" appeared ");
	}
	else if (this->values[this->idx] > 0 && this->values[this->idx] > value) {
		s.append(" decreased ");
	}
	else if (this->values[this->idx] > 0 && this->values[this->idx] < value) {
		s.append(" increased ");
	}
	else if (this->values[this->idx] > 0 && value == 0) {
		s.append(" disappeared ");
	}
	else {
		s.append(" changed ");
	}

	s.append("(" + to_string(this->values[this->idx]) + "->" + to_string(value) + ")");
	this->logs.push_back(s);
}