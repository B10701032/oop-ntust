#pragma once
#include "Diary.h"
#include <iostream>
#include <vector>
using namespace std;

class Creature {
public:
	Creature(string);
	Creature(string, const Creature&);
	Creature& operator[](string);
	Creature& operator=(int);
	Creature& operator+=(int);
	Creature& operator-=(int);
	void PrintStatus();
	void PrintLog();

private:
	int idx;
	string name;
	string lastDate;
	vector<string> logs;
	vector<string> parts;
	vector<int> values;
	void addToLogs(int);
};