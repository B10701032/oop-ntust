#ifndef PrintCombination_H
#define PrintCombination_H
#include <iostream>
using namespace std;
static void FindAllCombination(int arr[], int data[], int start, int end, int idx, int n) {
    // arr[] 總共有哪些元素
    // data[] 去存取每個組合
    // n 每個組合需要有幾個元素
    //樹枝法
    if (idx != n){
        for (int i = start; i < end + 1 and end - i + 1 >= n - idx ; i++){
            // 確保 i 小於array裡最大的idx值 
            //cout << i << " < " << end + 1 << "and " << end - i + 1 << " > " << n - idx + 1 << endl;
            //cout << " start: " << start << "\nend: " << end << endl;
            data[idx] = arr[i];
            //cout << "idx = " << idx << "\ndata[idx]= " << data[idx]<<endl;
            FindAllCombination(arr, data, i + 1, end, idx + 1, n);
        }
    }
    else {
        //當idx==n時代表組合已經有n個元素 輸出
        for (int j = 0; j < n; j++)
            cout << data[j] << " ";
        cout << std::endl;
    }
    return;
    
}
static void PrintCombination(int* arr, const int& r, const int& n) {
    int* data;
    //data 要放入 n 個元素
    data = new int[n] {0};
    FindAllCombination(arr, data, 0, r - 1, 0, n);
    //r-1代表 array 的最後一個idx 為 4(5-1)
};

#endif
