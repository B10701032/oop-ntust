#pragma once
#include<iostream>
#include<string>
//using namespace std;

namespace Authenticate {
	static std::string Passwords;
	static void inputPassword() {
		std::string input;
		bool refuse{ true };
		while (refuse) {
			int nonCharacters{ 0 };
			std::cout << "Enter your password (at least 8 characters and at least one non-letter)" << std::endl;
			std::cin >> input;
			for (unsigned int i = 0; i < input.size(); i++) {
				if (!isalpha(input[i])) nonCharacters++;
			}
			if (input.size() >= 8 and nonCharacters >= 1) refuse = false;
			else refuse = true;
		}
		Passwords = input;
	};
	static std::string getPassword() {
		return Passwords;
	};
}