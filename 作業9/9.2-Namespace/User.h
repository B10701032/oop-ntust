#pragma once
#include<iostream>
#include<string>
using namespace std;

namespace Authenticate {
	static std::string Name;
	static void inputUserName() {
		std::string N;
		bool reject = true;
		while (reject) {
			int nonalpha{ 0 };
			std::cout << "Enter your username (8 letters only)" << std::endl;
			std::cin >> N;
			//cout << N.size();
			for (unsigned int i = 0; i < N.size(); i++) {
				if (!isalpha(N[i])) nonalpha++;
			}
			//cout << Name.size() << endl;
			if (N.size() == 8 and nonalpha == 0) reject = false;
			else reject = true;
		}
		Name = N;
	};
	static std::string getUserName() {
		return Name;
	};
}