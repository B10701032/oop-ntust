#include "Graph.h";
#include <string>
#include <iostream>
using namespace std;
int main() {
	int n, m;
	Graph g(26);
	char a, b;
	string c, d;
	while (std::cin >> n >> m) {
		for (int i = 0; i < n; i++) {
			cin >> a >> b;
			g.addEdge(a, b);
		}
		for (int i = 0; i < m; i++) {
			cin >> c >> d;
			if (c == d){
				cout << "YES\n";
					continue;
			}
			if (c.length() != d.length()) {
				cout << "NO\n";
				continue;
			}
			bool InVail = true;
				for (int j = 0; j < c.length(); j++) {
					if (!g.isReachable(c[j]-'a', d[j]-'a')) {
						InVail = false;
						break;
					}
				}
			if (!InVail) cout << "YES";
			else cout << "NO";
		}
		
	}
	return 0;
}