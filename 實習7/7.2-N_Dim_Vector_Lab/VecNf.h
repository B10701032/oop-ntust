#include <iostream>
using namespace std;

class VecNf {
private:
	float *values;
	int dim;
public:
	VecNf();
	VecNf(float*, int);
	VecNf(const VecNf&);
	int Size();
	VecNf& operator=(const VecNf&);
	float operator[](int);
	VecNf operator+(const VecNf&);
	//friend VecNf& operator+(const VecNf&, const VecNf&);
	friend VecNf operator-(const VecNf&, const VecNf&);
	friend VecNf operator*(const VecNf&, const VecNf&);
	friend ostream& operator<<(ostream&, const VecNf&);
};