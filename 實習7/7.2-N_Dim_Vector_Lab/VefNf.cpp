#include "VecNf.h"


VecNf::VecNf() {
	this->values = new float[1]{ 0 };
	this->dim = 1;
}
VecNf::VecNf(float* values, int dim) {
	this->values = new float[dim];
	for (int i = 0; i < dim; i++)
	{
		this->values[i] = values[i];
	}
	this->dim = dim;
}
VecNf::VecNf(const VecNf& v) {
	this->values = new float[v.dim];
	for (int i = 0; i < v.dim; i++)
	{
		this->values[i] = v.values[i];
	}
	this->dim = v.dim;
}
VecNf& VecNf::operator=(const VecNf& v) {
	this->values = new float[v.dim];
	for (int i = 0; i < v.dim; i++)
	{
		this->values[i] = v.values[i];
	}
	this->dim = v.dim;
	return (*this);
}
float VecNf::operator[](int idx) {
	return this->values[idx];
}
//VecNf& operator+(const VecNf& a, const VecNf& b) {
//	if (a.dim != b.dim) {
//		cout << "Inconsistent";
//		VecNf obj;
//		return obj;
//	}
//	else {
//		float *values = new float[a.dim];
//		for (int i = 0; i < a.dim; i++)
//		{
//			values[i] = a.values[i] + b.values[i];
//		}
//		VecNf obj(values, a.dim);
//		return obj;
//	}
//}
VecNf VecNf::operator+(const VecNf& a) {
	if (a.dim != this->dim) {
		cout << "Inconsistent ";
		VecNf obj;
		return obj;
	}
	else {
		float* v = new float[this->dim];
		for (int i = 0; i < a.dim; i++)
		{
			v[i] = a.values[i] + this->values[i];
		}
		VecNf obj(v, a.dim);
		return obj;
		//return (*this);
	}
}
VecNf operator-(const VecNf& a, const VecNf& b) {
	if (a.dim != b.dim) {
		cout << "Inconsistent ";
		VecNf obj;
		return obj;
	}
	else {
		float* values = new float[a.dim];
		for (int i = 0; i < a.dim; i++)
		{
			values[i] = a.values[i] - b.values[i];
		}
		VecNf obj(values, a.dim);
		return obj;
	}
}
int VecNf::Size() {
	return dim;
}
VecNf operator*(const VecNf& a, const VecNf& b) {
	if (a.dim != b.dim) {
		cout << "Inconsistent ";
		VecNf obj;
		return obj;
	}
	else {
		float total = 0;
		for (int i = 0; i < a.dim; i++)
		{
			total += a.values[i] * b.values[i];
		}
		float* values = new float[1]{ total };
		VecNf obj(values, 1);
		return obj;
	}

}
ostream& operator<<(ostream& out, const VecNf& v) {
	for (int i = 0; i < v.dim; i++)
	{
		out << v.values[i] << " ";
	}
	return out;
}