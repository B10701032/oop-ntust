#include "Complex.h"
#include <string>

Complex::Complex() {
    realValue = 0;
    imaginaryValue = 0;
}
Complex::Complex(double r) {
    realValue = r;
    imaginaryValue = 0;
}
Complex::Complex(double r, double i) {
    realValue = r;
    imaginaryValue = i;
}
double Complex::real() {
    return realValue;
}
double Complex::imag() {
    return imaginaryValue;
}
double Complex::norm() {
    return sqrt(pow(realValue, 2) + pow(imaginaryValue, 2));
}
double real(Complex c) {
    return c.realValue;
}
double imag(Complex c) {
    return c.imaginaryValue;
}
double norm(Complex c) {
    return c.norm();
}
Complex Complex::operator+(Complex c) {
    c.realValue = realValue + c.realValue;
    c.imaginaryValue = imaginaryValue + c.imaginaryValue;
    return c;
}
Complex Complex::operator-(Complex c) {
    c.realValue = realValue - c.realValue;
    c.imaginaryValue = imaginaryValue - c.imaginaryValue;
    return c;
}
Complex Complex::operator*(Complex c) {
    c.realValue = (realValue * c.realValue) - (realValue * c.imaginaryValue);
    c.imaginaryValue = realValue * c.imaginaryValue - imaginaryValue * c.realValue;
    return c;
}
Complex Complex::operator/(Complex c) {
    double real_root = power(power(p.real, 2) + power(p.imginary, 2), -1);
    Complex c((real * p.real + imginary * p.imginary) * real_root, (-real * p.imginary + imginary * p.real) * real_root);
    return c;
}
Complex operator+(double d, Complex c) {
    //c.realValue = c.realValue + d;
    //c.imaginaryValue = c.imaginaryValue + d;
    return c + d;
}
Complex operator-(double d, Complex c) {
    //c.realValue = c.realValue - d;
    //c.imaginaryValue = c.imaginaryValue - d;
    return c - d;
}
Complex operator*(double d, Complex c) {
    //c.realValue = c.realValue * d;
    //c.imaginaryValue = c.imaginaryValue * d;
    return c * d;
}
Complex operator/(double d, Complex c) {
    //c.realValue = c.realValue / d;
    //c.imaginaryValue = c.imaginaryValue / d;
    return c / d;
}
bool operator==(Complex c1, Complex c2) {
    return c1.realValue == c2.realValue && c1.imaginaryValue == c2.imaginaryValue;
}
ostream& operator<<(ostream& strm, const Complex& c) {
    strm << c.realValue << " + " << c.imaginaryValue << "*i" << endl;
    return strm;
}
istream& operator>>(istream& strm, Complex& c) {
    string input;
    getline(strm, input);
    int realIdx = input.find('=');
    int imagIdx = input.find('+');
    c.realValue = atof(input.substr(realIdx + 1, imagIdx - realIdx).c_str());
    c.imaginaryValue = atof(input.substr(imagIdx + 1, input.length() - imagIdx - 1).c_str());
    return strm;
}