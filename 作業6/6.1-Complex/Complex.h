#include <iostream>

using namespace std;

class Complex
{
public:
	double realValue, imaginaryValue;

	Complex() {
		realValue = 0;
		imaginaryValue = 0;
	};
	//~Complex();
	Complex(double r) {
		realValue = r;
		imaginaryValue = 0;
	};
	//~Complex(double r);
	Complex(double r, double i) {
		realValue = r;
		imaginaryValue = i;
	};
	//~Complex(double r, double i);
	double real() {
		return realValue;
	};
	double imag() {
		return imaginaryValue;
	};
	double norm() {
		return sqrt(realValue * realValue + imaginaryValue * imaginaryValue);
	};
	friend double real(Complex c) {
		return c.realValue;
	};
	friend double imag(Complex c) {
		return c.imaginaryValue;
	};
	friend double norm(Complex c) {
		return sqrt(c.realValue * c.realValue + c.imaginaryValue * c.imaginaryValue);
	};
	Complex operator+(Complex c) {
		return Complex(realValue + c.realValue, imaginaryValue + c.imaginaryValue);
	};
	Complex operator-(Complex c) {
		return Complex(realValue - c.realValue, imaginaryValue - c.imaginaryValue);
	};
	Complex operator*(Complex c) {
		return Complex(realValue * c.realValue - imaginaryValue * c.imaginaryValue, realValue * c.imaginaryValue + imaginaryValue * c.realValue);
	};
	Complex operator/(Complex c) {
		double r = (realValue * c.realValue + imaginaryValue * (-c.imaginaryValue)) / (c.realValue * c.realValue + c.imaginaryValue * (-c.imaginaryValue));
		double i = (realValue * -c.imaginaryValue + imaginaryValue * c.realValue) / (c.realValue * c.realValue + c.imaginaryValue * (-c.imaginaryValue));
		return Complex(r, i);
	};
	friend Complex operator+(double d, Complex c);
	friend Complex operator-(double d, Complex c);
	friend Complex operator*(double d, Complex c);
	friend Complex operator/(double d, Complex c);
	friend bool operator==(Complex c1, Complex c2);	
	friend ostream& operator<<(ostream &strm, const Complex &c);
	friend istream& operator>>(istream &strm,Complex &c);
};
