#include <iostream>
#include <string>
#include <vector>
#include <math.h>
using namespace std;
int Min(int x, int y, int z) {
    if (x > y) {
        if (y > z) return z;
        else return y;
    }
    else { 
        if (x > z) return z;
        else return x;
    }
}
int main() {
    string input;
    string compare;
    int distance[100][100]{0};
    int tmp{0};
    while (getline(cin, input)) { 
        getline(cin, compare);
        for (int i = 0; i < compare.size()+2; i++) {
            distance[i][0] = i;
        }
        for (int i = 0; i < input.size()+1; i++) {
            distance[0][i] = i;
        }
        for (int i = 1; i < compare.size()+1; i++) {
            for (int j = 1; j < input.size()+1; j++) {
                if (compare[i-1] != input[j-1]) tmp = 1;
                else tmp = 0;
                distance[i][j] = Min(distance[i - 1][j] + 1, distance[i][j - 1] + 1, distance[i - 1][j - 1] + tmp);
            }
        }
        cout << distance[compare.size()][input.size()] << endl;
        cin.ignore(EOF, 0);
    }
}