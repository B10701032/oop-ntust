#include <iostream>
#include <string>
#include <math.h>
using namespace std;
int stringToInt(string s) {
	int res(0);
	for (int i = 0; i < s.length(); i++) {
		if (s[i] == 'U') res += pow(4, s.length() - i - 1);
		else if (s[i] == 'C') res += 2 * pow(4, s.length() - i - 1);
		else if (s[i] == 'D') res += 3 * pow(4, s.length() - i - 1);
	}
	return res;
}
int main() {
	int times;
	string s1, s2,ans;
	char op[3];
	while (cin >> times) {
		cout << "COWCULATIONS OUTPUT";
		for (int idx = 0; idx < times; idx++) {
			cin >> s1 >> s2;

			int n1 = stringToInt(s1);
			int n2 = stringToInt(s2);
			for (int i = 0; i < 3; i++) {
				cin >> op[i];
				if (op[i] == 'A') n2 += n1;
				else if (op[i] == 'R') n2 /= 4;
				else if (op[i] == 'L') n2 *= 4;
			}
			cin >> ans;
			int a = stringToInt(ans);
			if (n2 == a) cout << "YES\n";
			else cout << "NO\n";
		}
		
		cout << "END OF OUTPUT";
		return 0;
	}
	
}