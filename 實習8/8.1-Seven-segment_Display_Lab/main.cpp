#include <iostream>
#include <string>
#include <cstring>
using namespace std;

int main() {
    char input[100];
    bool dash(false);
    while (cin.getline(input,100)) {
        for (int i = 0; input[i]!='\0'; i++) {
            if (input[i] == '1') cout << "   ";
            else if (input[i] == '2') cout << " _ ";
            else if (input[i] == '3') cout << " _ ";
            else if (input[i] == '4') cout << "   ";
            else if (input[i] == '5') cout << " _ ";
            else if (input[i] == '6') cout << " _ ";
            else if (input[i] == '7') cout << " _ ";
            else if (input[i] == '8') cout << " _ ";
            else if (input[i] == '9') cout << " _ ";
            else if (input[i] == '0') cout << " _ ";
            else cout << "";
        }
        if (!dash) cout << endl;
        for (int i = 0; input[i] != '\0'; i++) {
            if (input[i] == '1') cout << "  |";
            else if (input[i] == '2') cout << " _|";
            else if (input[i] == '3') cout << " _|";
            else if (input[i] == '4') cout << "|_|";
            else if (input[i] == '5') cout << "|_ ";
            else if (input[i] == '6') cout << "|_ ";
            else if (input[i] == '7') cout << "  |";
            else if (input[i] == '8') cout << "|_|";
            else if (input[i] == '9') cout << "|_|";
            else if (input[i] == '0') cout << "| |";
            else cout << "";
            //cout << endl;
        }
        cout << endl;
        for (int i = 0; input[i] != '\0'; i++) {
            if (input[i] == '1') cout << "  |";
            else if (input[i] == '2') cout << "|_ ";
            else if (input[i] == '3') cout << " _|";
            else if (input[i] == '4') cout << "  |";
            else if (input[i] == '5') cout << " _|";
            else if (input[i] == '6') cout << "|_|";
            else if (input[i] == '7') cout << "  |";
            else if (input[i] == '8') cout << "|_|";
            else if (input[i] == '9') cout << " _|";
            else if (input[i] == '0') cout << "|_|";
            else cout << "";
        }
        cout << endl;
        cin.ignore(EOF,0);
    }
}