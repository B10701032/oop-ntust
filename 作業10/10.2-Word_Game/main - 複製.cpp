#include <iostream>
#include <fstream>
#include <string>
#include <vector>

using namespace std;
int main() {
    vector<string> words;
    string filename = "words.txt";
    string s, input[4];
    ifstream f(filename);
    if (f.is_open()) {
        while (getline(f, s)) {
            // at least three letters long
            if (s.length() > 2) {
                words.push_back(s);
            }
        }
    }
    char gameboard[4][4];

    while (getline(cin, input[0])) {
        //cout << input[0] << endl;
        if (input[0].empty()) break;
        getline(cin, input[1]);
        //cout << input[1] << endl;
        getline(cin, input[2]);
        //cout << input[2] << endl;
        getline(cin, input[3]);
        //cout << input[3] << endl;

        for (int i = 0; i < 4; i++) {
            for (int j = 0; j < 4; j++) {
                gameboard[i][j] = input[i][j];
                //cout << i << "," << j << ":" << gameboard[i][j] << " ";
            }
            //cout << endl;
        }

        // check every word
        for (int l = 0; l < words.size(); l++) {
            bool valid = false;
            vector<vector<int>> q;
            int idx, tempI, tempJ;
            for (int i = 0; i < 4; i++) {
                for (int j = 0; j < 4; j++) {
                    // check valid
                    if (gameboard[i][j] == words[l][0]) {
                        vector<int> t;
                        t.push_back((i * 4) + j);
                        q.push_back(t);

                         
                        while (!q.empty()) {
                            idx = q[0].size();
                            vector<int> possib;
                            tempI = q[0][q[0].size() - 1] / 4, tempJ = q[0][q[0].size() - 1] % 4;
                            //std::cout << tempI << ", " << tempJ << endl;
                            // up
                            if (tempI > 0 && gameboard[tempI - 1][tempJ] == words[l][idx]) {
                                possib.push_back(((tempI - 1) * 4) + tempJ);
                            }
                            // down
                            if (tempI < 3 && gameboard[tempI + 1][tempJ] == words[l][idx]) {
                                possib.push_back(((tempI + 1) * 4) + tempJ);
                            }
                            // left
                            if (tempJ > 0 && gameboard[tempI][tempJ - 1] == words[l][idx]) {
                                possib.push_back(((tempI) * 4) + tempJ - 1);
                            }
                            // right
                            if (tempJ < 3 && gameboard[tempI][tempJ + 1] == words[l][idx]) {
                                possib.push_back(((tempI) * 4) + tempJ + 1);
                            }
                            // diag up left
                            if (tempI > 0 && tempJ > 0 && gameboard[tempI - 1][tempJ - 1] == words[l][idx]) {
                                possib.push_back(((tempI - 1) * 4) + tempJ - 1);
                            }
                            // diag up right
                            if (tempI > 0 && tempJ < 3 && gameboard[tempI - 1][tempJ + 1] == words[l][idx]) {
                                possib.push_back(((tempI - 1) * 4) + tempJ + 1);
                            }
                            // diag down left
                            if (tempI < 3 && tempJ > 0 && gameboard[tempI + 1][tempJ - 1] == words[l][idx]) {
                                possib.push_back(((tempI + 1) * 4) + tempJ - 1);
                            }
                            // diag down right
                            if (tempI < 3 && tempJ < 3 && gameboard[tempI + 1][tempJ + 1] == words[l][idx]) {
                                possib.push_back(((tempI + 1) * 4) + tempJ + 1);
                            }

                            for (int m = 0; m < possib.size(); m++) {
                                //std::cout << "before possib:" << possib[m] << endl;
                                for (int k = 0; k < q[0].size(); k++) {
                                    if (q[0][k] == possib[m]) {
                                        possib.erase(possib.begin() + m);
                                        m--;
                                        break;
                                    }
                                }
                            }

                            for (int m = 0; m < possib.size(); m++) {
                                //std::cout << "after possib:" << possib[m] << endl;
                                vector<int> v;
                                for (int k = 0; k < q[0].size(); k++) {
                                    v.push_back(q[0][k]);
                                }
                                v.push_back(possib[m]);
                                q.push_back(v);
                            }
                            if (!possib.empty()) idx++;

                            // valid
                            if (idx >= words[l].length()) {
                                valid = true;
                                break;
                            }
                            q.erase(q.begin());
                        }

                    }
                }
                if (valid) break;
            }
            if (valid) {
                std::cout << words[l] << endl;
            }
        }
        std::cout << endl;
        cin.ignore(EOF, 0);
    }
    return 0;
}