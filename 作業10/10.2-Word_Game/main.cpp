#include<iostream>
#include<string>
#include<fstream>
#include<vector>
using namespace std;

const int step[8][2] = { {-1,-1},{0,-1},{1,-1},{-1,0},{1,0},{-1,1},{0,1},{1,1} };
//這個代表，基本上每個字會有八個鄰近的英文字母的idx
bool compareWord(vector<string> dictionary, int indexX, int indexY, int wordIndex,string word) {
	if (word.length() == wordIndex) return false; //這邊我還不知道是為什麼QQ
	dictionary[indexX][indexY] = 0; //把用過的字變成0，因為每一個字只能用一次
	int newX = -1, newY = -1, next = wordIndex + 1;
	//初始化座標
	for (int i = 0; i < 8; i++) {//去跑那八個可能的字
		newX = indexX + step[i][0];
		//原本字母所在的位置去做x座標和y座標的相加減
		if (newX < 0 || newX > 3) continue;//超過界線
		newY = indexY + step[i][1];
		if (newY < 0 || newY > 3) continue;//超過界線
		if (dictionary[newX][newY] == 0) continue;//若該格等於0就代表該字母已經用過了
		if (dictionary[newX][newY] == word[wordIndex]) {//如果一樣的話近一步的判斷
			if (word.length() == next) {
				//如果字母已經是最後一個話就代表已經找到組合
				cout << word << endl;
				return true;
			}
			if (compareWord(dictionary, newX, newY, next, word)) return true;
			//如果還不是的話就要繼續找下一個，這邊的wordIndex要記得+1變成next!
		}
		//cout << "第幾個字" << i << endl;
	}

	return false;
}
int main() {
	//開檔
	string file("words.txt");
	vector<string> datas;
	ifstream f(file);
	if(f) {
		string s = "";
		while (getline(f, s)) {
			datas.push_back(s);
			//把words.txt所有單字存好
		}
		f.close();
		vector<string> dictionary;
		//製作對照的字典
		int times = 4;
		//總共四次
		while (cin >> s) {
			dictionary.push_back(s);
			times--;
			if (times == 0) {//等於0時，代表已經輸入完畢，要開始找字
				for (int i = 0; i < datas.size(); i++) {
					//去跑每一個單字
					string word = datas[i];
					for (int j = 0, x = 0, y = 0; j < 16; j++, x = j / 4, y = j % 4) {
						//這邊WordGame也有一樣的用法，他是去找字典裡的每一個字
						if (word[0] == dictionary[x][y]) { //如果單字的第一個字跟字典裡面的字一樣的話
							if (compareWord(dictionary, x, y, 1, word)) {
								//開始進入迴圈去找每一個字是否都符合
								break;
							}
						}
					}
				}
				times = 4;//記得要把次數變回原來的樣子，不然會進入無限的迴圈
			}
		}
	}
}