#include <iostream>
#include <string>
using namespace std;
int main() {
    int m, n;
    char a = '*';
    char b = 'X';
    while (cin >> m >> n) {
        char** ary;
        ary = new char* [n];
        for (int i = 0; i < n; i++) {
            ary[i] = new char[m]();
        }
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < m; j++) {
                ary[i][j] = a;
                //cout << ary[i][j];
            }
            //cout << endl;
        }
        char shape;
        int w, x, y, z;
        string type;
        while (cin >> shape) {
            if (shape == 'S') {
                cin >> w >> x >> y;

                if (x + w > m || y + w > n) {
                    cout << "Out of range." << endl << endl;
                    continue;
                }
                for (int i = x; i < x + w; i++) {
                    for (int j = y; j < y + w; j++) {
                        ary[j][i] = b;
                    }
                }
                for (int i = 0; i < n; i++) {
                    for (int j = 0; j < m; j++) {
                        cout << ary[i][j];
                    }
                    cout << endl;
                }
                cout << endl;
            }
            else if (shape == 'T') {
                cin >> w >> x >> y >> type;
                if (x > m || y > n || w > n || w > m) {
                    cout << "Out of range." << endl << endl;
                    continue;
                }
                if (type == "LU") {
                    cout << x - w + 1 << " " << y - w + 1 << endl;
                    if (x - w + 1 >= 0 && y - w + 1 >= 0) {
                        int count = 0;
                        for (int i = y - w + 1; i <= y; ++i) {
                            count++;
                            for (int j = x; j > x - count; --j) {
                                cout << "( " << i << " , " << j << " )" << endl;
                                ary[i][j] = b;
                            }
                        }
                    }
                    else {
                        cout << "Out of range." << endl << endl;
                        continue;
                    }
                }
                else if (type == "LD") {
                    if (x - w + 1 >= 0 && y + w - 1 <= n) {
                        int count = w;
                        for (int i = y; i < y + w; ++i) {
                            count--;
                            for (int j = x; j >= x - count; --j) {
                                ary[i][j] = b;
                            }
                        }
                    }
                    else {
                        cout << "Out of range." << endl << endl;
                        continue;
                    }

                }
                else if (type == "RU") {
                    if (x + w - 1 <= m && y - w + 1 >= 0) {
                        int count = 0;
                        for (int i = y - w + 1; i <= y; ++i) {
                            count++;
                            for (int j = x; j < x + count; ++j) {
                                ary[i][j] = b;
                            }
                        }
                    }
                    else {
                        cout << "Out of range." << endl << endl;
                        continue;
                    }
                }
                else if (type == "RD") {
                    if (y + w - 1 <= n && x + w - 1 <= m) {
                        int count = w;
                        for (int i = y; i < y + w; ++i) {
                            count--;
                            for (int j = x + count; j >= x; --j) {
                                ary[i][j] = b;
                            }
                        }
                    }
                    else {
                        cout << "Out of range." << endl << endl;
                        continue;
                    }
                }
                for (int i = 0; i < n; i++) {
                    for (int j = 0; j < m; j++) {
                        cout << ary[i][j];
                    }
                    cout << endl;
                }
                cout << endl;
            }
            else if (shape == 'L') {
                cin >> w >> x >> y >> z;
                if (x >= m || z >= n || y >= m || w >= n) {
                    cout << "Out of range." << endl << endl;
                    continue;
                }
                if (w == y) {
                    for (int i = w; i <= z; i++) {
                        for (int j = x; j < x + 1; j++) {
                            ary[i][j] = b;
                        }
                    }
                }
                else if (x == z) {
                    for (int i = x; i < x + 1; i++) {
                        for (int j = w; j <= y; j++) {
                            ary[i][j] = b;
                        }
                    }
                }
                else if (w < y) {
                     if (x - z > 0) {
                         for (int i = 0; i <= x - z; i++) {
                             ary[x - i][w - i] = b;
                         }
                     }
                     else {
                         for (int i = 0; i <= z - x; i++) {
                             ary[x + i][w - i] = b;
                         }
                     }
                 }
                else if (w > y) {
                     if (x - z > 0) {
                         for (int i = 0; i <= x - z; i++) {
                             ary[x - i][w + i] = b;
                         }
                     }
                     else {
                         for (int i = 0; i <= z - x; i++) {
                             ary[x + i][w + i] = b;
                         }
                     }
                 }

                for (int i = 0; i < n; i++) {
                    for (int j = 0; j < m; j++) {
                        cout << ary[i][j];
                    }
                    cout << endl;
                }
                cout << endl;
            }
            else {
                break;
            }
            /*for (int i = 0; i < m; i++) {
                {
                    delete[] ary[i];
                }
                delete[] ary;
            }*/
        }
    }
    return 0;
}