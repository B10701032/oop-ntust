#include <iostream>
#include <math.h>
using namespace std;
int factorial(int i, unsigned long long int total, unsigned long long int maxRange) {
    if (maxRange < total) {
        return i - 1;
    }
    else {
        i++;
        return factorial(i, total * i, maxRange);
    }
}
int main() {
    int year;
    int bits(4);
    while (cin >> year) {
        if (1900 >= year > 1910) cout << 3 << endl;
        else if (1910 >= year > 1920) cout << 5 << endl;
        else if (1920 >= year > 1930) cout << 8 << endl;
        else if (1930 >= year > 1940) cout << 5 << endl;
        else if (1940 >= year > 1950) cout << 34 << endl;
        else if (1950 >= year > 1960) cout << 34 << endl;
        else if (1960 >= year > 1970) cout << 57 << endl;
        else if (1970 >= year > 1980) cout << 170 << endl;
        else if (1980 >= year > 1990) cout << 300 << endl;
        else if (1990 >= year > 2000) cout << 300 << endl;
        else if (2000 >= year > 2010) cout << 536 << endl;
        else if (2010 >= year > 2020) cout << 966 << endl;
        else if (2020 >= year > 2030) cout << "count" << endl;
        else if (2030 >= year > 2040) cout << 3210 << endl;
        else if (2040 >= year > 2050) cout << 1 << endl;
        else if (2050 >= year > 2060) cout << 1 << endl;
        else if (2060 >= year > 2070) cout << 1 << endl;
        else if (2070 >= year > 2080) cout << 1 << endl;
        else if (2080 >= year > 2090) cout << 1 << endl;
        else if (2090 >= year > 2100) cout << 1 << endl;
        else if (2100 >= year > 2110) cout << 1 << endl;
        else if (2110 >= year > 2120) cout << 536 << endl;
        else if (2120 >= year > 2130) cout << 966 << endl;
        else if (2130 >= year > 2140) cout << "count" << endl;
        else if (2140 >= year > 2150) cout << 3210 << endl;
        else if (2150 >= year > 2160) cout << 1 << endl;
        else if (2160 >= year > 2170) cout << 1 << endl;
        else if (2170 >= year > 2180) cout << 1 << endl;
        else if (2180 >= year > 2190) cout << 1 << endl;
        else if (2190 >= year > 2200) cout << 1 << endl;
    }
}