#include <iostream>
#include <string>
#include <sstream>
using namespace std;

#define CHAR 0
#define SHORT 1
#define INTEGER 2
#define STRING 3

void Set(char* memory, int N, stringstream& sstream, int cmd, int position)
{
	unsigned int value = 0;
	int err = 0, writeCount = 0;
	char ch = 0;
	switch (cmd)
	{
	case STRING:
		if (position >= N)
			throw "Violation Access.";
		while (~(ch = sstream.get()))
		{
			if (position < N)
				memory[position++] = ch;
			else
				throw "Violation Access.";
		}
		memory[position] = '\0';
		break;
	case CHAR: case SHORT: case INTEGER:
		writeCount = N - position;
		if (cmd == CHAR)
			if (writeCount >= 1)
				writeCount = 1;
			else
				err = 1;
		else if (cmd == SHORT)
			if (writeCount >= 2)
				writeCount = 2;
			else
				err = 1;
		else if (cmd == INTEGER)
			if (writeCount >= 4)
				writeCount = 4;
			else
				err = 1;
		sstream >> value;
		for (int i = 0; i < writeCount; i++)
		{
			memory[position + i] = value & 0xff;
			value >>= 8;
		}
		if (err)
			throw "Violation Access.";
		break;
	}
}

void Get(char* memory, int N, int cmd, int position)
{
	unsigned int value = 0;
	int readCount = 0;
	switch (cmd)
	{
	case STRING:
		cout << (string)(memory + position) << endl;
		break;
	case CHAR: case SHORT: case INTEGER:
		if (cmd == CHAR)
			readCount = 1;
		else if (cmd == SHORT)
			readCount = position + 2 <= N ? 2 : 0;
		else if (cmd == INTEGER)
			readCount = position + 4 <= N ? 4 : 0;

		if (position >= N or readCount == 0)
			throw "Violation Access.";

		for (int i = 0; i < readCount; i++)
			value |= (unsigned char)(memory[position + i]) << (i << 3);
		if (readCount != 0)
			cout << value << endl;
	}
}

void MemoryPointer(char* memory, int N)
{
	int position, isErr = 0;
	string cmd;
	stringstream sstream;

	getline(cin, cmd);
	sstream << cmd;
	sstream >> cmd >> position;
	if (position < 0)
		throw "Violation Access.";

	if (cmd == "Set")
	{
		sstream >> cmd;
		sstream.get();
		if (cmd == "char")
			Set(memory, N, sstream, CHAR, position);
		else if (cmd == "String")
			Set(memory, N, sstream, STRING, position);
		else if (cmd == "short")
			Set(memory, N, sstream, SHORT, position);
		else if (cmd == "int")
			Set(memory, N, sstream, INTEGER, position);
	}
	else if (cmd == "Get")
	{
		sstream >> cmd;
		if (cmd == "char")
			Get(memory, N, CHAR, position);
		else if (cmd == "String")
			Get(memory, N, STRING, position);
		else if (cmd == "short")
			Get(memory, N, SHORT, position);
		else if (cmd == "int")
			Get(memory, N, INTEGER, position);

	}
}

int main()
{
	int N = 0, K = 0;
	while (cin >> N)
	{
		cin >> K;
		char* memory = new char[N + 1]();
		cin.get();
		for (int i = 0; i < K; i++)
		{
			try
			{
				MemoryPointer(memory, N);
			}
			catch (char const* error)
			{
				cout << error << endl;
			}
		}
		delete[] memory;
	}
	return 0;
}