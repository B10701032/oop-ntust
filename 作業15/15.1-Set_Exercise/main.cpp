#include <iostream>
#include <set>
#include <fstream>
#include <string>
using namespace std;
int main() {
	set<string>::iterator iter;
	set<string> names;
	ifstream f("name.txt");
	if (f) {
		string s("");
		while (getline(f, s)) {
			names.insert(s);
		}
		f.close();
		for (iter = names.begin(); iter != names.end(); iter++) {
			cout << *iter << endl;
		}
	}
	else cout << "Can not find the file";
}