#include <iostream>
#include "Train.h"

using namespace std;

int main() {
    Train train;
    train.input();
    std::cout << (int)train.getVolume() << std::endl;
    return 0;
}