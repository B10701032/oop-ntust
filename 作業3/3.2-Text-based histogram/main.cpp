#include <iostream>
#include <cmath>
#include <iomanip>
using namespace std;

int main()

{	
	int nCnt[6] = { 0 };
	int n;
	while (cin >> n) {
		if (n == 0) nCnt[0]++;
		else if (n == 1) nCnt[1]++;
		else if (n == 2) nCnt[2]++;
		else if (n == 3) nCnt[3]++;
		else if (n == 4) nCnt[4]++;
		else if (n == 5) nCnt[5]++;
	}
	for (int i = 0; i < 6; i++) {
		cout << nCnt[i] << " grade(s) of " << i << endl;
	}
	return 0;
}