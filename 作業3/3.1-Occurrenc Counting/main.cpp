#include <iostream>
#include <vector>
using namespace std;
class Number {
public:
    float number;
    int Cnt;
};

int main() {
    vector<Number> number;
    float n;
    while (cin >> n) {
        bool add = true;
        for (int i = 0; i < number.size(); i++) {
            if (number[i].number == n) {
                number[i].Cnt++;
                add = false;
                break;
            }
        }
        if (add) {
            Number num;
            num.number = n;
            num.Cnt = 1;
            number.push_back(num);
            
        }
    }
    for (int i = 0; i < number.size(); i++) {
        for (int j = 0; j < number.size()-i-1; j++) {
            if (number[j].number < number[j + 1].number) {
                float tmpNmuber = number[j].number;
                int tmpCnt = number[j].Cnt;
                number[j].number = number[j + 1].number;
                number[j].Cnt = number[j + 1].Cnt;
                number[j + 1].number = tmpNmuber;
                number[j + 1].Cnt = tmpCnt;
            }
        }
    }
    cout << "N\tcount" <<endl;
    for (int i = 0; i < number.size(); i++) {
        cout << static_cast<int>( number[i].number ) << "\t" << static_cast<int>(number[i].Cnt) << endl;
    }
    return 0;
}