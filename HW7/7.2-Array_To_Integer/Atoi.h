#include <string>
#include <math.h>
#include <iostream>
using namespace std;

class Atoi {

private:
	string beTrans;

public:
	Atoi() {
		beTrans = "";
	};
	Atoi(string s) {
		beTrans = s;
	}
	void SetString(string s) {
		beTrans = s;
	};
	const string GetString() {
		return beTrans;
	}
	int Length() {
		if (beTrans[0] == '-') return beTrans.length() - 1;
		else if (beTrans == "0") return 1;
		else return beTrans.length();
	}
	bool IsDigital() {
		if (isdigit(beTrans[0]) || beTrans[0] == '-') {
			for (int i = 1; i < beTrans.length(); i++) {
				if (isdigit(beTrans[i]) == false) return false;
			}
			return true;
		}
		else return false;
	}
	int StringToInteger() {
		int result = 0;
		for (int i = 1; i < beTrans.length(); i++) {
			result = result + (static_cast<int>(beTrans[i] - '0') * pow(10, beTrans.length() - 1 - i));
		}
		if (beTrans[0] == '-') return -result;
		return result + (static_cast<int>(beTrans[0] - '0') * pow(10, beTrans.length() - 1));
	}

};