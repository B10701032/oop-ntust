#include<string>
#include<iostream>
#include<fstream>
#include<vector>
using namespace std;
class Form
{
private:

public:
	string input;
	string FileName;
	vector<char> dictionary;
	vector<int> alphNumOfInput;
	vector<string> CorrespondWord;
	vector<char> chDictionary;
	vector<int> alphNumOfch;

	void SetInputWord(string inputWord) {
		input = inputWord;
	};//設定Input
	void ProcessInputWord() {
		for (int i = 0; i < input.length(); i++){
			bool same = false;
			if (isupper(input[i])) input[i] = tolower(input[i]);
			for (int j = 0; j < dictionary.size(); j++) {
				if (input[i] == dictionary[j]) {
					same = true;
					alphNumOfInput[j]++;
				}
			}
			if (same == false) {
				dictionary.push_back(input[i]);
				alphNumOfInput.push_back(1);
			}
		}
	};//處理 Input 的資訊
	void SetFileName(string fileName) {
		FileName = fileName;
	};//設定檔案名稱
	void Load_CompareWord() {
		fstream file;
		char ch[40];
		file.open(FileName, ios::in);
		int i;
		do {
			file.getline(ch, sizeof(ch));
			for (i = 0; ch[i] != '\0'; i++) {
				bool same = false;
				for (int j = 0; j < chDictionary.size(); j++) {
					if (ch[i] == chDictionary[j]) {
						same = true;
						alphNumOfch[j]++;
					}
				}
				if (same == false) {
					chDictionary.push_back(ch[i]);
					alphNumOfch.push_back(1);
				}
			};// ch的字典
			isSameWord(ch, dictionary, alphNumOfInput, chDictionary, alphNumOfch);
			chDictionary.clear();
			alphNumOfch.clear();
		} while (i!=0);

		
	};//讀檔並且比較
	void PrintFoundWords() {
		for (int i = 0; i < CorrespondWord.size(); i++) {
			cout << CorrespondWord[i] << endl;
		}
	};//印出符合的字
	void isSameWord(char ch[], vector<char> dictionary, vector<int> alphNumOfInput, vector<char> chDictionary, vector<int> alphNumOfch) {
		bool enough = false;
		if (chDictionary.size() <= dictionary.size()) {
			for (int i = 0; i < chDictionary.size(); i++) {
				enough = false;
				for (int j = 0; j < dictionary.size(); j++) {
					if (chDictionary[i] == dictionary[j]) {
						if (alphNumOfch[i] > alphNumOfInput[j]) {
							enough = false;
							break;
						}
						else enough = true;
					}
				}
				if (!enough) break;
			}
			if (enough) CorrespondWord.push_back(ch);
		}
	}
};
