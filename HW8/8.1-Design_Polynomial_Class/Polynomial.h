#include <math.h>
class Polynomial {
private :
public:
	double* coefficient;
	int Size;
	Polynomial() {
		this->Size = 1;
		coefficient = new double[Size] {0};
	}
	Polynomial(double* param, int Size) {
		this->Size = Size;
		this->coefficient = new double[Size];
		for (int i = 0; i < Size; i++) {
			this->coefficient[i] = param[i];
		}
	}
	Polynomial(Polynomial const& rhs) {
		this->Size = rhs.Size;
		this->coefficient = new double[Size];
		for (int i = 0; i < Size; i++) {
			this->coefficient[i] = rhs.coefficient[i];
		}
	}
	int mySize() {
		return Size;
	}
	friend Polynomial operator+(Polynomial const& rhs, const double& d) {
		double* coefficient;
		int Size = rhs.Size;
		coefficient = new double[Size] {0};
		for (int i = 0; i < Size; i++) {
			coefficient[i] = rhs.coefficient[i];
		}
		coefficient[0] += d;
		return Polynomial(coefficient, Size);
	}
	friend Polynomial operator+(const double& d, Polynomial const& rhs) {
		double* coefficient;
		int Size = rhs.Size;
		coefficient = new double[Size] {0};
		for (int i = 0; i < Size; i++) {
			coefficient[i] = rhs.coefficient[i];
		}
		coefficient[0] += d;
		return Polynomial(coefficient, Size);
	}
	friend Polynomial operator-(Polynomial const& rhs, const double& d) {
		double* coefficient;
		int Size = rhs.Size;
		coefficient = new double[Size] {0};
		for (int i = 0; i < Size; i++) {
			coefficient[i] = rhs.coefficient[i];
		}
		coefficient[0] -= d;
		return Polynomial(coefficient, Size);
	}
	friend Polynomial operator-(const double& d, Polynomial const& rhs) {
		double* coefficient;
		int Size = rhs.Size;
		coefficient = new double[Size] {0};
		for (int i = 0; i < Size; i++) {
			coefficient[i] = -rhs.coefficient[i];
		}
		coefficient[0] = coefficient[0]+d;
		return Polynomial(coefficient, Size);
	}
	friend Polynomial operator+(Polynomial const& lhs, Polynomial const& rhs) {
		double* coefficient;
		int Max,Min;
		if (lhs.Size > rhs.Size) {
			Max = lhs.Size;
			Min = rhs.Size;
		}
		else {
			Max = rhs.Size;
			Min = lhs.Size;
		}
		int i(0);
		coefficient = new double[Max] {0};
		for (i = 0; i < Min; i++) {
			coefficient[i] = lhs.coefficient[i] + rhs.coefficient[i];
			//std::cout << "i= " << i <<  "lhs.coefficient[i]= " << lhs.coefficient[i] << " " << "rhs.coefficient[i]= " << rhs.coefficient[i] << std::endl;
		}
		for (i=Min; i < Max; i++) {
			if (Max == lhs.Size) coefficient[i] = lhs.coefficient[i];
			else coefficient[i] = rhs.coefficient[i];
		}
		return Polynomial(coefficient, Max);
	}
	friend Polynomial operator-(Polynomial const& lhs, Polynomial const& rhs) {
		double* coefficient;
		int Max, Min;
		if (lhs.Size > rhs.Size) {
			Max = lhs.Size;
			Min = rhs.Size;
		}
		else {
			Max = rhs.Size;
			Min = lhs.Size;
		}
		int i(0);
		coefficient = new double[Max] {0};
		for (i = 0; i < Min; i++) {
			coefficient[i] = lhs.coefficient[i] - rhs.coefficient[i];
		}
		for (i=Min; i < Max; i++) {
			if (Max == lhs.Size) coefficient[i] = lhs.coefficient[i];
			else coefficient[i] = -rhs.coefficient[i];
		}
		return Polynomial(coefficient, Max);
	}
	Polynomial& operator=(Polynomial const& rhs) {
		if (this == &rhs) return *this;
		delete[] this->coefficient;
		this->Size = rhs.Size;
		this->coefficient = new double[Size];
		for (int i = 0; i < Size; i++) {
			this->coefficient[i] = rhs.coefficient[i];
		}
		return *this;
	}
	double& operator[](int i) {
		//std::cout << i << " " << Size - i-1 << std::endl;
 		return coefficient[i];
	}
	friend Polynomial operator*(Polynomial const& lhs, Polynomial const& rhs) {
		double* coefficient;
		int Size = (lhs.Size-1) + (rhs.Size-1)+1;
		coefficient = new double[Size]{0};
		for (int i = 0; i < rhs.Size; i++) {
			for (int j = 0; j < lhs.Size; j++) {
				coefficient[i + j] += rhs.coefficient[i] * lhs.coefficient[j];
				//std::cout << i + j <<" "<< coefficient[i+j]<<std::endl;
			}
		}
		return Polynomial(coefficient, Size);
	}
	friend Polynomial operator*(Polynomial const& rhs, double d) {
		double* coefficient;
		int Size = rhs.Size;
		coefficient = new double[Size];
		for (int i = 0; i < rhs.Size; i++) {
			coefficient[i] = rhs.coefficient[i] * d;
		}
		return Polynomial(coefficient, Size);
	}
	friend Polynomial operator*(double d, Polynomial const& rhs) {
		double* coefficient;
		int Size = rhs.Size;
		coefficient = new double[Size];
		for (int i = 0; i < rhs.Size; i++) {
			coefficient[i] = rhs.coefficient[i] * d;
		}
		return Polynomial(coefficient, Size);
	}
};
double evaluate(const Polynomial& poly, const double& var) {
	double res(0);
	for (int i = 0; i < poly.Size; i++) {
		res += poly.coefficient[i] * pow(var, i);
		//std::cout << "i = " << i-1 << " " << poly.coefficient[i] << " var=" << var << " power = " << poly.Size - i - 1 << std::endl;
	}
	return res;
}