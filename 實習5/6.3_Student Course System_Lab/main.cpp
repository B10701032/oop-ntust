#include <iostream>
#include <string>
#include <vector>
using namespace std;

const string DAY_CODE = "MTWRF\0";

struct Course {
    string name;
    vector<string> schedules;
};

struct Student {
    string name;
    vector<Course> courses;
};

int findStudent(vector<Student> students, string studentName) {
    if (students.empty()) return -1;
    int i = 0;
    for (; i < students.size(); i++)
    {
        if (students[i].name == studentName) {
            break;
        }
    }
    if (i < students.size()) {
        return i;
    }
    else {
        return -1;
    }
}

int findCourse(vector<Course> courses, string courseName) {
    if (courses.empty()) return -1;
    int i = 0;
    for (; i < courses.size(); i++)
    {
        if (courses[i].name == courseName) {
            break;
        }
    }
    if (i < courses.size()) {
        return i;
    }
    else {
        return -1;
    }
}

void fillSchedule(int(*schedule)[5][10], vector<Course> courses) {
    for (int i = 0; i < courses.size(); i++) {
        for (int j = 0; j < courses[i].schedules.size(); j++) {
            for (int k = 0; k < 5; k++) {
                if (courses[i].schedules[j][0] == DAY_CODE[k]) {
                    int time = atoi(courses[i].schedules[j].substr(1, courses[i].schedules[j].length() - 1).c_str());
                    (*schedule)[k][time - 1] = i + 1;
                    break;
                }
            }
        }
    }
}

string trim(string s) {
    int startIdx = s.find_first_not_of(" \t\n\r\f\v");
    int endIdx = s.find_last_not_of(" \t\n\r\f\v");
    return s.substr(startIdx == string::npos ? 0 : startIdx, endIdx == string::npos ? s.length() : endIdx + 1);
}

int main() {
    vector<Student> students;
    string input;
    while (getline(cin, input)) {
        input = trim(input);
        int commandIdx = input.find(' ') + 1;
        string command = input.substr(0, commandIdx - 1);
        if (command == "AddStudent") {
            string studentName = input.substr(commandIdx, input.length() - commandIdx);
            int studentIdx = findStudent(students, studentName);
            if (studentIdx >= 0) cout << "The student's name is duplicate." << endl;
            else {
                Student newStudent;
                newStudent.name = studentName;
                students.push_back(newStudent);
            }
        }
        else if (command == "AddCourse") {
            int nameIdx = input.find(' ', commandIdx) + 1;
            string studentName = input.substr(commandIdx, nameIdx - commandIdx - 1);
            int studentIdx = findStudent(students, studentName);
            if (studentIdx < 0) cout << "The student's name does not exist." << endl;
            else {
                int courseIdx = input.find(' ', nameIdx) + 1;
                string courseName = studentName = input.substr(nameIdx, courseIdx - nameIdx - 1);
                Course newCourse;
                newCourse.name = courseName;
                int nextCourseIdx = input.find(' ', courseIdx);
                vector<string> intCourseTimes;
                bool conflict = false;
                while (true) {
                    if (nextCourseIdx == string::npos) {
                        nextCourseIdx = input.length();
                    }
                    string courseTime = input.substr(courseIdx, nextCourseIdx - courseIdx);
                    newCourse.schedules.push_back(courseTime);
                    if (nextCourseIdx == input.length()) break;
                    else {
                        courseIdx = nextCourseIdx + 1;
                        nextCourseIdx = input.find(' ', courseIdx);
                    }
                }
                // gotta sort this shit
                vector<string> sortedSchedules;
                for (int i = 0; i < DAY_CODE.length(); i++) {
                    for (int j = 1; j <= 10; j++) {
                        string courseCode = DAY_CODE.substr(i, 1).append(to_string(j));
                        for (int k = 0; k < newCourse.schedules.size(); k++) {
                            if (courseCode == newCourse.schedules[k]) {
                                sortedSchedules.push_back(courseCode);
                                newCourse.schedules.erase(newCourse.schedules.begin() + k);
                                break;
                            }
                        }
                        if (newCourse.schedules.empty()) break;
                    }
                    if (newCourse.schedules.empty()) break;
                }
                newCourse.schedules = sortedSchedules;
                if (students[studentIdx].courses.empty()) {
                    students[studentIdx].courses.push_back(newCourse);
                }
                else {
                    for (int i = 0; i < students[studentIdx].courses.size(); i++) {
                        for (int j = 0; j < students[studentIdx].courses[i].schedules.size(); j++) {
                            for (int k = 0; k < newCourse.schedules.size(); k++) {
                                if (students[studentIdx].courses[i].schedules[j] == newCourse.schedules[k]) {
                                    conflict = true;
                                    break;
                                }
                            }
                            if (conflict) break;
                        }
                        if (conflict) break;
                    }
                    if (conflict) {
                        cout << "Course conflict." << endl;
                    }
                    else {
                        students[studentIdx].courses.push_back(newCourse);
                    }

                }
            }
        }
        else if (command == "DelStudent") {
            string studentName = input.substr(commandIdx, input.length() - commandIdx);
            int studentIdx = findStudent(students, studentName);
            if (studentIdx < 0) cout << "The student's name does not exist." << endl;
            else {
                students.erase(students.begin() + studentIdx);
            }
        }
        else if (command == "DelCourse") {
            int nameIdx = input.find(' ', commandIdx) + 1;
            string studentName = input.substr(commandIdx, nameIdx - commandIdx - 1);
            int studentIdx = findStudent(students, studentName);
            if (studentIdx < 0) cout << "The student's name does not exist." << endl;
            else {
                string courseName = input.substr(nameIdx, input.length() - nameIdx);
                int courseIdx = findCourse(students[studentIdx].courses, courseName);
                if (courseIdx < 0) cout << "The course does not exist." << endl;
                else {
                    students[studentIdx].courses.erase(students[studentIdx].courses.begin() + courseIdx);
                }
            }

        }
        else if (command == "Print") {
            int studentIdx = input.find(' ', commandIdx);
            string studentName;

            if (studentIdx == string::npos) {
                studentName = input.substr(commandIdx, input.length() - commandIdx);
                if (studentName == "StudentList") {
                    if (students.empty()) {
                        cout << "The Students list is empty." << endl;
                    }
                    else {
                        for (int i = 0; i < students.size(); i++) {
                            cout << students[i].name << endl;
                        }
                    }
                }
                else {
                    studentIdx = findStudent(students, studentName);
                    if (studentIdx < 0) cout << "The student's name does not exist." << endl;
                    else {
                        int schedule[5][10];
                        for (int i = 0; i < 5; i++) {
                            for (int j = 0; j < 10; j++) {
                                schedule[i][j] = 0;
                            }
                        }
                        fillSchedule(&schedule, students[studentIdx].courses);
                        for (int i = 0; i < 5; i++) {
                            cout << DAY_CODE[i] << ":";
                            for (int j = 0; j < 10; j++) {
                                if (schedule[i][j] > 0) {
                                    cout << " " << j + 1 << ":" << students[studentIdx].courses[schedule[i][j] - 1].name;
                                }
                            }
                            cout << endl;
                        }
                    }
                }
            }
            else {
                studentIdx++;
                studentName = input.substr(commandIdx, studentIdx - commandIdx - 1);
                string courseName = input.substr(studentIdx, input.length() - studentIdx);
                studentIdx = findStudent(students, studentName);
                if (studentIdx < 0) cout << "The student's name does not exist." << endl;
                else {
                    int courseIdx = findCourse(students[studentIdx].courses, courseName);
                    if (courseIdx < 0) cout << "The course does not exist." << endl;
                    else {
                        cout << students[studentIdx].courses[courseIdx].name ;
                        for (int i = 0; i < students[studentIdx].courses[courseIdx].schedules.size(); i++) {
                            cout << " " << students[studentIdx].courses[courseIdx].schedules[i];
                        }
                        cout << endl;
                    }
                }
            }
        }
    }

    return 0;
}