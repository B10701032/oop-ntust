#include <iostream>
#include <string>
#include <vector>
using namespace std;

struct Student {
    string id;
    string name;
    string gender;
    float height;
    float weight;
};

void printStudent(Student s) {
    cout << s.id << "\t";
    cout << s.name << "\t";
    cout << s.gender << "\t";
    cout << s.height << "\t";
    cout << s.weight << endl;
}

string trim(string s) {
    int startIdx = s.find_first_not_of(" \t\n\r\f\v");
    int endIdx = s.find_last_not_of(" \t\n\r\f\v");
    return s.substr(startIdx == string::npos ? 0 : startIdx, endIdx == string::npos ? s.length() : endIdx + 1);
}

int main() {
    vector<Student> students;
    string input;

    while (getline(cin, input)) {
        if (input.length() <= 0) break;

        if (input.substr(0, 4) == "List") {
            for (int i = 0; i < students.size(); i++) {
                printStudent(students[i]);
            }
        }
        else {
            int idIdx;
            idIdx = input.find(' ') + 1;
            string command = input.substr(0, idIdx - 1);
            string id;
            if (command == "Add") {
                int nameIdx, genderIdx, heightIdx, weightIdx;
                nameIdx = input.find(' ', idIdx) + 1;
                id = trim(input.substr(idIdx, nameIdx - idIdx - 1));

                genderIdx = input.find(' ', nameIdx) + 1;
                heightIdx = input.find(' ', genderIdx) + 1;
                weightIdx = input.find(' ', heightIdx) + 1;

                Student newStudent;
                newStudent.id = id;
                newStudent.name = input.substr(nameIdx, genderIdx - nameIdx - 1);
                newStudent.gender = input.substr(genderIdx, heightIdx - genderIdx - 1);
                newStudent.height = atof(input.substr(heightIdx, weightIdx - heightIdx - 1).c_str());
                newStudent.weight = atof(input.substr(weightIdx, input.length() - weightIdx).c_str());

                int i = 0;
                if (students.empty() || students[students.size() - 1].id.compare(newStudent.id) < 0) {
                    students.push_back(newStudent);
                }
                else if (students[i].id.compare(newStudent.id) > 0) {
                    students.insert(students.begin(), newStudent);
                }
                else {
                    bool duplicate = false;

                    for (; i < (students.size()); i++) {
                        if (students[i].id == id) {
                            cout << "The student's ID is duplicate." << endl;
                            duplicate = true;
                            break;
                        }
                        else if (students[i].id.compare(id) < 0) {
                            if (i < students.size() - 1 && students[i + 1].id.compare(id) > 0) {
                                i++;
                                break;
                            }
                        }

                    }
                    if (duplicate) continue;
                    else if (i > 0) {
                        students.insert(students.begin() + i, newStudent);
                    }
                }

            }
            else if (command == "Edit") {
                int fieldIdx, valueIdx;
                string field, value;
                int studentIdx = -1;
                fieldIdx = input.find(' ', idIdx) + 1;
                valueIdx = input.find(' ', fieldIdx) + 1;
                id = input.substr(idIdx, fieldIdx - idIdx - 1);
                field = input.substr(fieldIdx, valueIdx - fieldIdx - 1);
                value = trim(input.substr(valueIdx, input.length() - valueIdx));
                for (int i = 0; i < students.size(); i++) {
                    if (students[i].id == id) {
                        studentIdx = i;
                        break;
                    }
                }
                if (studentIdx < 0) {
                    cout << "Student Not Found." << endl;
                    continue;
                }
                if (field == "Name") {
                    students[studentIdx].name = value;
                }
                else if (field == "Gender") {
                    students[studentIdx].gender = value;
                }
                else if (field == "Height") {
                    students[studentIdx].height = atof(value.c_str());
                }
                else if (field == "Weight") {
                    students[studentIdx].weight = atof(value.c_str());
                }
                //cout << "Changed Succesfully" << endl;
            }
            else if (command == "Del") {
                bool found = false;
                id = trim(input.substr(idIdx, input.length() - idIdx));
                for (int i = 0; i < students.size(); i++) {
                    if (students[i].id == id) {
                        /*cout << "deleted " << students[i].id << "\t" << students[i].name << "\t" << students[i].gender <<
                         "\t" << students[i].height << "\t" << students[i].weight << endl;*/
                        students.erase(students.begin() + i);
                        found = true;
                        break;
                    }
                }
                if (!found) cout << "Student Not Found." << endl;
            }
            else if (command == "Find") {
                bool found = false;
                id = trim(input.substr(idIdx, input.length() - idIdx));
                for (int i = 0; i < students.size(); i++) {
                    if (students[i].id == (id)) {
                        printStudent(students[i]);
                        found = true;
                        break;
                    }
                }
                if (!found) cout << "Student Not Found." << endl;
            }
        }

    }

    return 0;
}